package com.school.journal.mapper;

import com.school.journal.dto.RelationshipDto;
import com.school.journal.model.users.Child;
import com.school.journal.model.users.Parent;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = { SchoolClassMapper.class, ParentMapper.class })
public interface RelationshipMapper {

    RelationshipMapper MAPPER = Mappers.getMapper(RelationshipMapper.class);

    @Mappings({
            @Mapping(target = "newChild.id", source = "child.id"),
            @Mapping(target = "newChild.lastName", source = "child.lastName"),
            @Mapping(target = "newChild.firstName", source = "child.firstName"),
            @Mapping(target = "newChild.patronymic", source = "child.patronymic"),
            @Mapping(target = "newChild.nickname", source = "child.nickname"),
            @Mapping(target = "newChild.email", source = "child.email"),
            @Mapping(target = "newChild.phoneNumber", source = "child.phoneNumber"),
            @Mapping(target = "newChild.schoolClassId", source = "child.schoolClass.id"),
    })
    RelationshipDto toDto(Child child, List<Parent> parents);

}
