package com.school.journal.mapper;

import com.school.journal.dto.ChildRowDto;
import com.school.journal.model.users.Child;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(uses = {SchoolClassMapper.class})
public interface ChildRowMapper {

    ChildRowMapper MAPPER = Mappers.getMapper(ChildRowMapper.class);

    ChildRowDto toDto(Child child);

    @InheritInverseConfiguration
    Child fromDto(ChildRowDto dto);

}
