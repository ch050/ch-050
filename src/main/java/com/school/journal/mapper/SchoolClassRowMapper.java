package com.school.journal.mapper;

import com.school.journal.dto.SchoolClassRowDto;
import com.school.journal.model.users.SchoolClass;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper
public interface SchoolClassRowMapper {

    SchoolClassRowMapper MAPPER = Mappers.getMapper(SchoolClassRowMapper.class);

    @Mappings({
            @Mapping(target = "curatorId", source = "schoolClass.curator.id"),
            @Mapping(target = "curatorLastName", source = "schoolClass.curator.lastName"),
            @Mapping(target = "curatorFirstName", source = "schoolClass.curator.firstName"),
            @Mapping(target = "curatorPatronymic", source = "schoolClass.curator.patronymic"),
            @Mapping(target = "startDate", source = "schoolClass.studyingStart"),
            @Mapping(target = "endDate", source = "schoolClass.studyingEnd")
    })
    SchoolClassRowDto toDto(SchoolClass schoolClass);

    @InheritInverseConfiguration
    SchoolClass fromDto(SchoolClassRowDto dto);
}
