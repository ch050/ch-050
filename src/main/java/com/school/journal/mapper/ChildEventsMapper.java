package com.school.journal.mapper;

import com.school.journal.dto.ChildEventDto;
import com.school.journal.model.events.LessonEvent;

import java.util.List;
import java.util.stream.Collectors;

public class ChildEventsMapper {

    public static List<ChildEventDto> toDto(List<LessonEvent> lessonEvents) {
        return lessonEvents.stream().map(ChildEventsMapper::toDto).collect(Collectors.toList());
    }

    public static ChildEventDto toDto(LessonEvent lessonEvent) {
        ChildEventDto childEventDto = new ChildEventDto();

        childEventDto.setSubject(lessonEvent.getLesson().getSubject().getName());
        childEventDto.setComment(lessonEvent.getComment());
        childEventDto.setFiles(lessonEvent.getFiles());

        return childEventDto;
    }

}
