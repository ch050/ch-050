package com.school.journal.mapper;

import com.school.journal.dto.ParentDto;
import com.school.journal.model.users.Parent;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = {ChildMapper.class})
public interface ParentMapper {

    ParentMapper MAPPER = Mappers.getMapper(ParentMapper.class);

    @Mapping(target = "phone", source = "phoneNumber")
    ParentDto toDto(Parent parent);

    @InheritInverseConfiguration
    Parent fromDto(ParentDto dto);
}
