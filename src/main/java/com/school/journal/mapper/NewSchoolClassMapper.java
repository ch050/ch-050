package com.school.journal.mapper;

import com.school.journal.dto.NewSchoolClassDto;
import com.school.journal.model.users.SchoolClass;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper(uses = {SchoolClassMapper.class})
public interface NewSchoolClassMapper {

    NewSchoolClassMapper MAPPER = Mappers.getMapper(NewSchoolClassMapper.class);

    @Mappings({
            @Mapping(target = "curatorId", source = "schoolClass.curator.id")
    })
    NewSchoolClassDto toDto(SchoolClass schoolClass);

    @InheritInverseConfiguration
    SchoolClass fromDto(NewSchoolClassDto dto);

}
