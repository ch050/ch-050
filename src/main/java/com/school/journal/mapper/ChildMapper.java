package com.school.journal.mapper;

import com.school.journal.dto.NewChildDto;
import com.school.journal.model.users.Child;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper(uses = {SchoolClassMapper.class})
public interface ChildMapper {

    ChildMapper MAPPER = Mappers.getMapper(ChildMapper.class);

    @Mappings({
            @Mapping(target = "schoolClassId", source = "child.schoolClass.id")
    })
    NewChildDto toDto(Child child);

    @InheritInverseConfiguration
    Child fromDto(NewChildDto dto);

}
