package com.school.journal.mapper;

import com.school.journal.dto.SchoolClassDto;
import com.school.journal.model.users.SchoolClass;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface SchoolClassMapper {

    SchoolClassMapper MAPPER = Mappers.getMapper(SchoolClassMapper.class);

    SchoolClassDto toDto(SchoolClass schoolClass);

    @InheritInverseConfiguration
    SchoolClass fromDto(SchoolClassDto dto);

}
