package com.school.journal.mapper;

import com.school.journal.dto.ParentRowDto;
import com.school.journal.model.users.Parent;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ParentRowMapper {

    ParentRowMapper MAPPER = Mappers.getMapper(ParentRowMapper.class);

    ParentRowDto toDto(Parent parent);

    @InheritInverseConfiguration
    Parent fromDto(ParentRowDto dto);


}
