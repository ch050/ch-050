package com.school.journal.model.users;


import com.school.journal.model.security.Authority;
import com.school.journal.model.security.User;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Collection;

@Entity
@DiscriminatorValue("teacher")
public class Teacher extends User implements Serializable {

    @Column(name = "description")
    private String description;

    public Teacher() {
    }

    public Teacher(String description) {
        this.description = description;
    }

    public Teacher(String id, String nickname,
                   String password, boolean active,
                   String firstName, String patronymic,
                   String lastName, String phoneNumber,
                   Collection<Authority> authorities,
                   String description, String email) {
        super(id, nickname, password,
                active, firstName, patronymic,
                lastName, phoneNumber,
                email, authorities);
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
