package com.school.journal.model.users;

import com.school.journal.model.security.Authority;
import com.school.journal.model.security.User;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "parent")
@NamedQueries({
        @NamedQuery(name = Parent.FIND_ACTIVE,
                query = "FROM Parent p WHERE p.active = true")
})
public class Parent extends User implements Serializable {

    public static final String FIND_ACTIVE = "Parent.findActive";

    @Column(name = "job", length = 64)
    private String job;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "relationship",
            joinColumns = {@JoinColumn(name = "parent_id")},
            inverseJoinColumns = {@JoinColumn(name = "child_id")})
    private List<Child> children;

    public Parent() {
    }

    public Parent(String job) {
        this.job = job;
    }

    public Parent(String id, String nickname,
                  String password, boolean active,
                  String firstName, String patronymic,
                  String lastName, String phoneNumber,
                  Collection<Authority> authorities, String job,
                  String email
    ) {
        super(id, nickname, password, active, firstName, patronymic, lastName, phoneNumber,
                email, authorities);
        this.job = job;
    }

    public List<Child> getChildren() {
        return children;
    }

    public void setChildren(List<Child> children) {
        this.children = children;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

}
