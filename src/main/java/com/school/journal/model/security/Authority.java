package com.school.journal.model.security;


import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

@Entity
@Table(name = "sec_authority")
public class Authority implements GrantedAuthority {

    public enum AuthorityType {
        TEACHER, ADMIN, PARENT, CHILD
    }

    @Id
    @Column(name = "name", nullable = false, length = 36)
    @Enumerated(EnumType.STRING)
    private AuthorityType authority;

    @Column(name = "page", length = 36)
    private String defaultPage;

    public Authority(AuthorityType authority, String defaultPage) {
        this.authority = authority;
        this.defaultPage = defaultPage;
    }

    public Authority() {
    }

    public String getDefaultPage() {
        return defaultPage;
    }

    public void setDefaultPage(String defaultPage) {
        this.defaultPage = defaultPage;
    }

    @Override
    public String getAuthority() {
        return authority.name();
    }

    public void setAuthority(AuthorityType authority) {
        this.authority = authority;
    }

}
