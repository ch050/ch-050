package com.school.journal.model.security;

import java.util.Collection;

public class UserIdentity {

    private String id;

    private String nickname;

    private String password;

    private boolean active;

    private String firstName;

    private String patronymic;

    private String lastName;

    private String phoneNumber;

    private Collection<Authority> authorities;

    public UserIdentity(String id, String nickname,
                        String password, boolean active,
                        String firstName, String patronymic,
                        String lastName, String phoneNumber,
                        Collection<Authority> authorities
    ) {
        this.id = id;
        this.nickname = nickname;
        this.password = password;
        this.active = active;
        this.firstName = firstName;
        this.patronymic = patronymic;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.authorities = authorities;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Collection<Authority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Collection<Authority> authorities) {
        this.authorities = authorities;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
