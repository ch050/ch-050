package com.school.journal.model.security;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Table(name = "sec_user")
@NamedQueries({
        @NamedQuery(name = User.FIND_BY_USERNAME,
                query = "FROM User u WHERE u.nickname = :nickname")
})
@Inheritance(strategy = InheritanceType.JOINED)
public class User implements Serializable {

    public static final String FIND_BY_USERNAME = "User.findByUsername";

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id")
    private String id;

    @Column(name = "nickname", nullable = false, length = 36)
    private String nickname;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "active", nullable = false)
    private boolean active;

    @Column(name = "first_name", nullable = false, length = 64)
    private String firstName;

    @Column(name = "patronymic", length = 64)
    private String patronymic;

    @Column(name = "last_name", nullable = false, length = 64)
    private String lastName;

    @Column(name = "phone_number", length = 36)
    private String phoneNumber;

    @Column(name = "email", length = 36)
    private String email;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "sec_user_authority",
            joinColumns = {@JoinColumn(name = "user_id", nullable = false)},
            inverseJoinColumns = {@JoinColumn(name = "authority_name", nullable = false)})
    private Collection<Authority> authorities;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public User() {
    }

    public User(String id, String nickname,
                String password, boolean active,
                String firstName, String patronymic,
                String lastName, String phoneNumber,
                String email, Collection<Authority> authorities
    ) {
        this.id = id;
        this.nickname = nickname;
        this.password = password;
        this.active = active;
        this.firstName = firstName;
        this.patronymic = patronymic;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.authorities = authorities;
        this.email = email;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Collection<Authority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Collection<Authority> authorities) {
        this.authorities = authorities;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

}
