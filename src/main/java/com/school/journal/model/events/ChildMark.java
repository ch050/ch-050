package com.school.journal.model.events;

import com.school.journal.model.users.Child;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "child_mark",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"lesson_event_id", "child_id"})})
@NamedQueries({
        @NamedQuery(name = "ChildMark.getMarksByChildId",
                query = "FROM ChildMark c WHERE c.child.id = :childId")
})
public class ChildMark implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", length = 36)
    private String id;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "lesson_event_id")
    private LessonEvent lessonEvent;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "child_id")
    private Child child;

    @Column(name = "mark")
    private Integer mark;

    @Column(name = "absent")
    private Boolean absent;

    public ChildMark(LessonEvent lessonEvent, Child child, Integer mark, boolean absent) {
        this.lessonEvent = lessonEvent;
        this.child = child;
        this.mark = mark;
        this.absent = absent;
    }

    public ChildMark() {
    }

    public ChildMark(Integer mark, Boolean absent) {
        this.mark = mark;
        this.absent = absent;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LessonEvent getLessonEvent() {
        return lessonEvent;
    }

    public void setLessonEvent(LessonEvent lessonEvent) {
        this.lessonEvent = lessonEvent;
    }

    public Child getChild() {
        return child;
    }

    public void setChild(Child child) {
        this.child = child;
    }

    public Integer getMark() {
        return mark;
    }

    public String getStringMark() {
        if (mark == null)
            return "";
        return String.valueOf(mark);
    }

    public void setMark(Integer mark) {
        this.mark = mark;
    }

    public Boolean getAbsent() {
        return absent;
    }

    public void setAbsent(Boolean absent) {
        this.absent = absent;
    }
}

