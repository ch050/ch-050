package com.school.journal.model.events;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "lesson_event")
@NamedQueries({
        @NamedQuery(name = LessonEvent.FIND_BY_CHILD_ID_AND_DATE, query = "" +
                "select ev FROM LessonEvent ev " +
                "LEFT JOIN FETCH ev.files " +
                "LEFT JOIN FETCH ev.lesson l " +
                "INNER JOIN l.schoolClass sc " +
                "INNER JOIN sc.children ch " +
                "WHERE ch.id = :child_id AND ev.eventDate = :date"),
        @NamedQuery(name = LessonEvent.SELECT_JOURNAL ,
                query = " FROM LessonEvent le" +
                        " WHERE le.lesson.subject.id = :subjectId " +
                        " AND le.lesson.schoolClass.id = :classId " +
                        " AND le.lesson.teacher.id = :teacherId" +
                        " AND le.eventDate < :endDate " +
                        " AND le.eventDate > :startDate "),

})
public class LessonEvent implements Serializable {

    public static final String SELECT_JOURNAL = "LessonEvent.selectJournal";
    public static final String FIND_BY_CHILD_ID_AND_DATE = "LessonEvent.findByChildAndDate";

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", length = 36)
    private String id;

    @ManyToOne(fetch = FetchType.LAZY)
    private Lesson lesson;

    @Column(name = "event_date")
    private LocalDate eventDate;

    @Column(name = "comment")
    private String comment;

    @ManyToOne(fetch = FetchType.LAZY)
    private LessonEventType lessonEventType;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "lessonEvent")
    private List<ChildMark> childMarkList;

    @Column(name = "completed")
    private Boolean completed;

    @Column(name = "completion_date")
    private LocalDate completionDate;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "events")
    private List<FileResource> files;

    public LessonEvent() {
    }

    public LessonEventType getLessonEventType() {
        return lessonEventType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Lesson getLesson() {
        return lesson;
    }

    public void setLesson(Lesson lesson) {
        this.lesson = lesson;
    }

    public LocalDate getEventDate() {
        return eventDate;
    }

    public void setEventDate(LocalDate eventDate) {
        this.eventDate = eventDate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setLessonEventType(LessonEventType lessonEventType) {
        this.lessonEventType = lessonEventType;
    }

    public List<ChildMark> getChildMarkList() {
        return childMarkList;
    }

    public void setChildMarkList(List<ChildMark> childMarkList) {
        this.childMarkList = childMarkList;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public LocalDate getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(LocalDate completionDate) {
        this.completionDate = completionDate;
    }

    public List<FileResource> getFiles() {
        return files;
    }

    public void setFiles(List<FileResource> files) {
        this.files = files;
    }
}
