package com.school.journal.model.events;

import com.school.journal.model.users.SchoolClass;
import com.school.journal.model.users.Teacher;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "lesson",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"subject_id", "teacher_id", "class_id"})})
@NamedQueries({
        @NamedQuery(name = Lesson.FIND_BY_TEACHER,
                query = "FROM Lesson l WHERE l.teacher.id = :teacherId"),
        @NamedQuery(name = Lesson.FIND_BY_PARAMETERS,
                query = "FROM Lesson l WHERE l.teacher.id = :teacherId " +
                        "AND l.schoolClass.id = :classId " +
                        "AND l.subject.id = :subjectId "),
        @NamedQuery(name = Lesson.FIND_BY_SUBJECT,
                query = "FROM Lesson l WHERE l.teacher.id = :teacherId " +
                        "AND l.subject.id = :subjectId ")
})
public class Lesson implements Serializable {

    public static final String FIND_BY_TEACHER = "Lesson.findByTeacher";
    public static final String FIND_BY_PARAMETERS = "Lesson.findByParameters";
    public static final String FIND_BY_SUBJECT = "Lesson.findBySubject";

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", length = 36)
    private String id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "subject_id")
    private Subject subject;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "teacher_id")
    private Teacher teacher;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "class_id")
    private SchoolClass schoolClass;

    public Lesson() {
    }

    public Lesson(String id, Subject subject, Teacher teacher, SchoolClass schoolClass) {
        this(subject, teacher, schoolClass);
        this.id = id;
    }

    public Lesson(Subject subject, Teacher teacher, SchoolClass schoolClass) {
        this.subject = subject;
        this.teacher = teacher;
        this.schoolClass = schoolClass;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public SchoolClass getSchoolClass() {
        return schoolClass;
    }

    public void setSchoolClass(SchoolClass schoolClass) {
        this.schoolClass = schoolClass;
    }

}
