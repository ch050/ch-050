package com.school.journal.service.interfaces;

import com.school.journal.dto.NewChildDto;
import com.school.journal.model.users.Child;

import java.util.List;

public interface ChildService {

    String create(Child child);
    void update(NewChildDto child);
    Child getById(String id);
    void deactivate(Child child);
    List<Child> getAll();
    List<Child> getAllActive();
    String prepare(Child child);

}