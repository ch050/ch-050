package com.school.journal.service.interfaces;


import com.school.journal.model.security.User;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface UserService {

    List<User> getUsers();
    void updatePersonalInfo(User user, User newUser);

}
