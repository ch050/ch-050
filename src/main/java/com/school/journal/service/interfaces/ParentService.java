package com.school.journal.service.interfaces;

import com.school.journal.dto.ParentDto;
import com.school.journal.model.users.Parent;

import java.util.List;

public interface ParentService {

    void create(Parent parent);
    void update(ParentDto parentDto);
    Parent getById(String id);
    List<Parent> getAll();
    List<Parent> getAllActive();
    void prepare(Parent parent);
    void deactivate(Parent parent);
}
