package com.school.journal.service.interfaces;

import com.school.journal.dto.*;
import com.school.journal.model.events.LessonEvent;
import org.springframework.security.core.userdetails.User;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;


public interface LessonEventService {

    List<LessonDto> getTeachersLessons(User principal);

    List<ChildDto> getChild(String classId);

    String createLessonEvent(LessonEventDto lessonEvent, User principal);

    JournalDto createChildMark(JournalDto journalDto);

    ChildEvent getChildEvent(LessonEventDto lessonEventDto);

    List<LessonEvent> getEventsByChildIdAndDate(String childId, LocalDate date);

}
