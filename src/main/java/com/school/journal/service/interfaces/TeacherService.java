package com.school.journal.service.interfaces;

import com.school.journal.dto.TeacherDto;
import com.school.journal.model.users.Teacher;

import java.util.List;

public interface TeacherService {

    List<TeacherDto> getTeacher();

    void deleteTeacher(String teacherId);

    Teacher createTeacher(TeacherDto teacherDto);

    void updateTeacher(TeacherDto teacherDto);
}
