package com.school.journal.service.interfaces;

import com.school.journal.model.events.FileResource;

import java.util.Arrays;
import java.util.List;

public interface FileService {
    void saveFile(String subject, String eventId, byte[] file, String filename);
    FileResource getFileById(String id);
    String getUploadRootPath();
    void rename(String fileId, String newName);
    void removeFileById(String fileId);
    List<FileResource> getFilesByEventId(String eventId);

    static boolean isFileExtensionAllowed(String filename, String allowedExtensions) {
        List<String> extensions = Arrays.asList(allowedExtensions.split(","));
        String extension = FileService.getExtension(filename, true);

        return extensions.contains(extension);
    }
    static String getExtension(String filename) {
        return getExtension(filename, false);
    }
    static String getExtension(String filename, boolean withDot) {
        if (filename.lastIndexOf(".") != -1 && filename.lastIndexOf(".") != 0)
            return filename.substring(filename.lastIndexOf(".") + (withDot ? 0 : 1));
        else return "";
    }
}