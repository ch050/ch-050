package com.school.journal.service.interfaces;

import com.school.journal.dto.SubjectDto;
import com.school.journal.model.events.Subject;

import java.util.List;

public interface SubjectService {

    List<SubjectDto> getSubjects();

    void deleteSubject(String subjectId);

    Subject createSubject(SubjectDto subjectDto);

    void updateSubject(SubjectDto subjectDto);
}
