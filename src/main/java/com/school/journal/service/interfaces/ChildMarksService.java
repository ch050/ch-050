package com.school.journal.service.interfaces;

import com.school.journal.dto.*;

public interface ChildMarksService {

    ParentPageDto getInfo();
}
