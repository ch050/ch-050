package com.school.journal.service.interfaces;

import com.school.journal.dto.TeacherJournalDto;
import com.school.journal.dto.TeacherLessonEventDto;

public interface TeacherJournalService {

    TeacherJournalDto getJournal(TeacherLessonEventDto lessonEventDto);

}
