package com.school.journal.service.interfaces;

import com.school.journal.dto.NewSchoolClassDto;
import com.school.journal.model.users.SchoolClass;

import java.util.List;

public interface SchoolClassService {

    void add(NewSchoolClassDto schoolClassDto);
    SchoolClass getById(String id);
    List<SchoolClass> getAll();
    List<SchoolClass> getAllActive();
    void deactivate(SchoolClass schoolClass);
}
