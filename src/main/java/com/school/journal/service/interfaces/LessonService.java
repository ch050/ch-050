package com.school.journal.service.interfaces;

import com.school.journal.dto.*;
import com.school.journal.model.events.Lesson;

import java.util.List;

public interface LessonService {

    List<LessonPageDto> getAllLessons();

    List<SchoolClassDto> getSchoolClass();

    void deleteLessons(String lessonId);

    Lesson createLesson(LessonPageDto lessonDto);

    void updateLesson(LessonPageDto lessonDto);
}
