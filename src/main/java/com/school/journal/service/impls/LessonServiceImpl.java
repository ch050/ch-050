package com.school.journal.service.impls;

import com.school.journal.dao.interfaces.LessonDao;
import com.school.journal.dao.interfaces.SchoolClassDao;
import com.school.journal.dao.interfaces.SubjectDao;
import com.school.journal.dao.interfaces.TeacherDao;
import com.school.journal.dto.LessonPageDto;
import com.school.journal.dto.SchoolClassDto;
import com.school.journal.dto.TeacherDto;
import com.school.journal.model.events.Lesson;
import com.school.journal.model.events.Subject;
import com.school.journal.model.users.SchoolClass;
import com.school.journal.model.users.Teacher;
import com.school.journal.service.interfaces.LessonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service(value = "LessonService")
@Transactional(readOnly = true)
public class LessonServiceImpl implements LessonService {

    @Autowired
    private LessonDao lessonDao;

    @Autowired
    private SchoolClassDao schoolClassDao;

    @Override
    public List<LessonPageDto> getAllLessons() {
        ArrayList<LessonPageDto> lessonPageDtos = new ArrayList<>();
        for (Lesson lesson : lessonDao.findAll()) {
            lessonPageDtos.add(new LessonPageDto(lesson));
        }
        return lessonPageDtos;
    }

    @Override
    public List<SchoolClassDto> getSchoolClass() {
        ArrayList<SchoolClassDto> schoolClassDtos = new ArrayList<>();
        for (SchoolClass schoolClass : schoolClassDao.findAll()){
            schoolClassDtos.add(new SchoolClassDto(schoolClass));
        }
        return schoolClassDtos;
    }

    @Transactional
    @Override
    public void deleteLessons(String lessonId) {
        lessonDao.deleteById(lessonId);
    }

    @Transactional
    @Override
    public Lesson createLesson(LessonPageDto lessonDto) {
        Subject subject = new Subject();
        Teacher teacher = new Teacher();
        SchoolClass schoolClass = new SchoolClass();
        schoolClass.setId(lessonDto.getSchoolClass().getId());
        teacher.setId(lessonDto.getTeacher().getId());
        subject.setId(lessonDto.getSubject().getId());
        Lesson lesson = new Lesson(subject,teacher, schoolClass);
        lessonDao.create(lesson);
     return lesson;
    }

    @Transactional
    @Override
    public void updateLesson(LessonPageDto lessonDto) {
        Subject subject = new Subject();
        Teacher teacher = new Teacher();
        SchoolClass schoolClass = new SchoolClass();
        schoolClass.setId(lessonDto.getSchoolClass().getId());
        teacher.setId(lessonDto.getTeacher().getId());
        subject.setId(lessonDto.getSubject().getId());
        lessonDao.update(new Lesson(lessonDto.getId(), subject, teacher, schoolClass));
    }


}
