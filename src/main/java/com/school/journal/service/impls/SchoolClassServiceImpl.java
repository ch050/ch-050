package com.school.journal.service.impls;

import com.school.journal.dao.interfaces.SchoolClassDao;
import com.school.journal.dto.NewSchoolClassDto;
import com.school.journal.mapper.NewSchoolClassMapper;
import com.school.journal.model.users.SchoolClass;
import com.school.journal.service.interfaces.SchoolClassService;
import com.school.journal.validation.SchoolClassValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class SchoolClassServiceImpl implements SchoolClassService {

    @Autowired
    private SchoolClassDao schoolClassDao;

    @Transactional
    public void add(NewSchoolClassDto schoolClassDto) {
        SchoolClass schoolClass = NewSchoolClassMapper.MAPPER.fromDto(schoolClassDto);
        if (!SchoolClassValidation.isClassNameCorrect(schoolClass.getName())) {
            return;
        }
        schoolClass.setActive(true);
        schoolClassDao.create(schoolClass);
    }

    @Override
    public SchoolClass getById(String id) {
        return schoolClassDao.findOne(id);
    }

    @Override
    public List<SchoolClass> getAll() {
        return schoolClassDao.findAll();
    }

    @Override
    public List<SchoolClass> getAllActive() {
        return schoolClassDao.getActiveClasses();
    }

    @Transactional
    public void deactivate(SchoolClass schoolClass) {
        schoolClass.setActive(false);
        schoolClassDao.update(schoolClass);
    }


}
