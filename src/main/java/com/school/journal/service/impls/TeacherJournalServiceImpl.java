package com.school.journal.service.impls;

import com.school.journal.dao.interfaces.LessonEventDao;
import com.school.journal.dao.interfaces.UserDao;
import com.school.journal.dto.ChildJournal;
import com.school.journal.dto.TeacherJournalDto;
import com.school.journal.dto.TeacherLessonEventDto;
import com.school.journal.dto.TypeLessonEventDto;
import com.school.journal.model.events.LessonEvent;
import com.school.journal.model.users.Child;
import com.school.journal.service.interfaces.TeacherJournalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class TeacherJournalServiceImpl implements TeacherJournalService {

    @Autowired
    private LessonEventDao lessonEventDao;

    @Autowired
    private UserDao userDao;

    @Transactional
    @Override
    public TeacherJournalDto getJournal(TeacherLessonEventDto lessonEventDto) {
        List<LessonEvent> journal = lessonEventDao.getJournal(lessonEventDto,
                userDao.findUserByUsername(UserDetailsService.getPrincipal().getUsername()).getId());
        TeacherJournalDto teacherJournalDto = new TeacherJournalDto();
        if (!journal.isEmpty() && !journal.get(0).getLesson().getSchoolClass().getChildren().isEmpty()) {
            teacherJournalDto.setChildJournals(new ArrayList<>());
            teacherJournalDto.setTypeLessonEvents(new ArrayList<>());
            initChildren(journal.get(0).getLesson().getSchoolClass().getChildren(), teacherJournalDto);
            initTypeLessonEvent(journal, teacherJournalDto);
        }
        return teacherJournalDto;
    }

    private void initChildren(List<Child> children, TeacherJournalDto teacherJournalDto) {
        for (int i = 0; i < children.size(); i++) {
            teacherJournalDto.getChildJournals().add(new ChildJournal());
            teacherJournalDto.getChildJournals().get(i).setChildName(
                    children.get(i).getFirstName() + " " +
                            children.get(i).getPatronymic() + " " +
                            children.get(i).getLastName() + " "
            );
            teacherJournalDto.getChildJournals().get(i).setMarks(new ArrayList<>());
        }
    }

    private void initTypeLessonEvent(List<LessonEvent> journal, TeacherJournalDto teacherJournalDto) {
        for (LessonEvent event : journal) {
            teacherJournalDto.getTypeLessonEvents()
                    .add(new TypeLessonEventDto(event.getLessonEventType().getName(), event.getEventDate()));
            for (int i = 0; i < event.getChildMarkList().size(); i++) {
                if (event.getChildMarkList().get(i).getAbsent() != null && event.getChildMarkList().get(i).getAbsent().equals(false)) {
                    teacherJournalDto.getChildJournals().get(i).getMarks().add("H");
                } else if (event.getChildMarkList().get(i).getMark() != null) {
                    teacherJournalDto.getChildJournals().get(i).getMarks()
                            .add(String.valueOf(event.getChildMarkList().get(i).getMark()));
                } else {
                    teacherJournalDto.getChildJournals().get(i).getMarks().add(" ");
                }
            }
        }
    }

}
