package com.school.journal.service.impls;

import com.school.journal.dao.interfaces.SubjectDao;
import com.school.journal.dto.SubjectDto;
import com.school.journal.model.events.Subject;
import com.school.journal.service.interfaces.SubjectService;
import com.school.journal.validation.SubjectValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
public class SubjectServiceImpl implements SubjectService{

    @Autowired
    private SubjectDao subjectDao;

    @Override
    public List<SubjectDto> getSubjects() {
     return subjectDao
             .findAll()
             .stream()
             .map(SubjectDto :: new)
             .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public void deleteSubject(String subjectId) {
        subjectDao.deleteById(subjectId);
    }

    @Transactional
    @Override
    public Subject createSubject(SubjectDto subjectDto) {
        Subject subject = new Subject(subjectDto.getName());
        if ((!SubjectValidator.validate(subject))
                || (!subjectDao.findByName(subjectDto.getName()).isEmpty())) {
            return null;
        }
        subjectDao.create(subject);
        return subject;
    }

    @Transactional
    @Override
    public void updateSubject(SubjectDto subjectDto) {
        Subject subject = new Subject(subjectDto.getId(),subjectDto.getName());
        if ((!SubjectValidator.validate(subject))
                || (subjectDao.findByName(subject.getName()) != null)) {
            return;
        }
        subjectDao.update(subject);
    }

}
