package com.school.journal.service.impls;

import com.school.journal.dao.interfaces.AuthorityDao;
import com.school.journal.dao.interfaces.ParentDao;
import com.school.journal.dto.ParentDto;
import com.school.journal.mapper.ParentMapper;
import com.school.journal.model.security.Authority;
import com.school.journal.model.users.Parent;
import com.school.journal.service.interfaces.ParentService;
import com.school.journal.service.interfaces.UserService;
import com.school.journal.utils.PasswordGenerator;
import com.school.journal.validation.UserValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class ParentServiceImpl implements ParentService {

    @Value("${security.password.length}")
    private int passwordLength;

    @Autowired
    private UserService userService;

    @Autowired
    private ParentDao parentDao;

    @Autowired
    private AuthorityDao authorityDao;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Transactional
    public void create(Parent parent) {
        prepare(parent);
        parentDao.update(parent);
    }

    @Transactional
    public void update(ParentDto parentDto) {
        Parent newParent = ParentMapper.MAPPER.fromDto(parentDto);
        Parent parent = getById(parentDto.getId());

        userService.updatePersonalInfo(parent, newParent);
        parent.setJob(newParent.getJob());

        if (!UserValidation.checkParent(parent)) {
            return;
        }

        parentDao.update(parent);
    }

    public Parent getById(String id) {
        return parentDao.findOne(id);
    }

    public List<Parent> getAll() {
        return parentDao.findAll();
    }

    public List<Parent> getAllActive() {
        return parentDao.getActiveParents();
    }

    public void prepare(Parent parent) {
        PasswordGenerator password = new PasswordGenerator.PasswordGeneratorBuilder()
                .useDigits(true).useLower(true).useUpper(true).build();
        String generatedPasswordForParent = password.generate(passwordLength);

        parent.setAuthorities(Collections.singletonList(authorityDao.findOne(Authority.AuthorityType.PARENT)));
        parent.setActive(true);
        parent.setPassword(passwordEncoder.encode(generatedPasswordForParent));
    }

    @Transactional
    public void deactivate(Parent parent) {
        parent.setActive(false);
        parentDao.update(parent);
    }
}
