package com.school.journal.service.impls;

import com.school.journal.dao.interfaces.*;
import com.school.journal.dto.*;
import com.school.journal.model.events.ChildMark;
import com.school.journal.model.security.User;
import com.school.journal.model.users.Parent;
import com.school.journal.service.interfaces.ChildMarksService;
import com.school.journal.utils.ParentPageDtoBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class ChildMarksServiceImpl implements ChildMarksService{

    @Autowired
    private UserDao userDao;

    @Autowired
    private ParentDao parentDao;

    @Autowired
    private ChildMarkDao childMarkDao;

    @Override
    public ParentPageDto getInfo() {
        User user = userDao.findUserByUsername(UserDetailsService.getPrincipal().getUsername());
        Parent parent = parentDao.findOne(user.getId());
        List<ChildMark> childMarks = childMarkDao.findAll();
        return new ParentPageDtoBuilder(parent, childMarks).build();
    }
}
