package com.school.journal.service.impls;

import com.school.journal.dao.interfaces.UserDao;
import com.school.journal.model.security.User;
import com.school.journal.service.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service(value = "UserService")
@Transactional(readOnly = true)
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    public List<User> getUsers() {
        return userDao.findAll();
    }

    public void updatePersonalInfo(User user, User newUser) {
        user.setLastName(newUser.getLastName());
        user.setFirstName(newUser.getFirstName());
        user.setPatronymic(newUser.getPatronymic());
        user.setNickname(newUser.getNickname());
        user.setEmail(newUser.getEmail());
        user.setPhoneNumber(newUser.getPhoneNumber());
    }

}
