package com.school.journal.service.impls;

import com.school.journal.dao.interfaces.FileDAO;
import com.school.journal.dao.interfaces.LessonEventDao;
import com.school.journal.model.events.FileResource;
import com.school.journal.model.events.LessonEvent;
import com.school.journal.service.interfaces.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

@Service
@Transactional(readOnly = true)
public class FileServiceImpl implements FileService {
    private static final Logger logger = Logger.getLogger(FileServiceImpl.class.getName());
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

    @Value("${upload.base_directory}")
    private String baseDirectory;

    @Autowired
    private FileDAO fileDAO;

    @Autowired
    private LessonEventDao lessonEventDao;

    @Transactional
    @Override
    public void saveFile(String subject, String eventId, byte[] file, String filename) {
        String relativeDirectory = File.separator + subject + File.separator + generateDate();

        File destinationDirectory = new File(getUploadRootPath() + relativeDirectory);
        destinationDirectory.mkdirs();

        String generatedFilename = generateName();
        String fullPath = Paths.get(destinationDirectory.getAbsolutePath()).toAbsolutePath() + File.separator + generatedFilename;

        StringBuilder messageBuilder = new StringBuilder();
        try {
            Files.write(Paths.get(fullPath).normalize(), file);

            FileResource fileResourceEntity = new FileResource();
            fileResourceEntity.setEvents(new ArrayList<LessonEvent>() {{
                add(lessonEventDao.findOne(String.valueOf(eventId)));
            }});
            fileResourceEntity.setName(filename);
            fileResourceEntity.setRelativePath(relativeDirectory + File.separator + generatedFilename);

            fileDAO.create(fileResourceEntity);

            messageBuilder.append("file saved");
        } catch (Exception e) {
            e.printStackTrace();
            messageBuilder.append("Fail to save file");
        }

        messageBuilder
                .append(", Path: ").append(fullPath)
                .append(", Subject: ").append(subject)
                .append(", Event id: ").append(eventId);

        logger.info(messageBuilder.toString());
    }

    @Override
    public FileResource getFileById(String id) {
        return fileDAO.findOne(id);
    }

    @Override
    public String getUploadRootPath() {
        return System.getProperty("catalina.home") + File.separator + baseDirectory;
    }

    @Transactional
    @Override
    public void rename(String fileId, String newName) {
        FileResource fileResource = fileDAO.findOne(fileId);
        fileResource.setName(newName);
        fileDAO.update(fileResource);
    }

    @Transactional
    @Override
    public void removeFileById(String fileId) {
        FileResource entityFileResource = fileDAO.findOne(fileId);

        if (entityFileResource != null) {
            logger.info("Removed file with id:" + fileId + ", name: " + entityFileResource.getName());

            String relativePath = entityFileResource.getRelativePath();

            Paths.get(getUploadRootPath(), relativePath).toFile().delete();
            fileDAO.deleteById(fileId);
        }

    }

    @Override
    public List<FileResource> getFilesByEventId(String eventId) {
        return fileDAO.getFilesByEventId(eventId);
    }

    private String generateDate() {
        return dateFormat.format(new Date());
    }

    private String generateName() {
        return UUID.randomUUID().toString().replace("-", "");
    }

}