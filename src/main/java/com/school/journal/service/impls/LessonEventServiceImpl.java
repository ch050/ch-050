package com.school.journal.service.impls;

import com.school.journal.dao.interfaces.*;
import com.school.journal.dto.*;
import com.school.journal.model.events.ChildMark;
import com.school.journal.model.events.Lesson;
import com.school.journal.model.events.LessonEvent;
import com.school.journal.model.events.LessonEventType;
import com.school.journal.model.users.Child;
import com.school.journal.service.interfaces.LessonEventService;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class LessonEventServiceImpl implements LessonEventService {

    @Autowired
    private LessonDao lessonDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private ChildDao childDao;

    @Autowired
    private LessonEventDao lessonEventDao;

    @Autowired
    private LessonEventTypeDao lessonEventTypeDao;

    @Override
    public List<ChildDto> getChild(String classId) {
        List<Child> children = childDao.getChildrenByClass(classId);
        List<ChildDto> childDtos = new ArrayList<>();
        for (Child child : children) {
            childDtos.add(new ChildDto(child));
        }
        return childDtos;
    }

    @Override
    public List<LessonDto> getTeachersLessons(User principal) {
        List<Lesson> lessons = lessonDao.getLessonsByTeacherId(userDao.findUserByUsername(principal.getUsername()));
        List<LessonDto> lessonDtos = new ArrayList<>();
        for (Lesson lesson : lessons) {
            LessonDto lessonDto = new LessonDto();
            lessonDto.setSubject(new SubjectDto(lesson.getSubject()));
            List<Lesson> lessonsBySubject = lessonDao.getLessonsBySubject(
                    userDao.findUserByUsername(UserDetailsService.getPrincipal().getUsername()).getId(),
                    lesson.getSubject().getId());
            lessonDto.setSchoolClasses(new ArrayList<>());
            for (Lesson lessonBySubject : lessonsBySubject) {
                lessonDto.getSchoolClasses().add(new SchoolClassDto(lessonBySubject.getSchoolClass()));
            }
            List<LessonEventTypeDto> types = new ArrayList<>();
            for (LessonEventType lessonEventType :
                    lessonEventTypeDao.findAll()) {
                types.add(new LessonEventTypeDto(lessonEventType));

            }
            lessonDto.setTypeDto(types);
            if (isContains(lessonDtos, lessonDto)) {
                lessonDtos.add(lessonDto);
            }
        }
        return lessonDtos;
    }

    @Transactional
    @Override
    public String createLessonEvent(LessonEventDto lessonEventDto, User principal) {
        LessonEvent lessonEvent = new LessonEvent();
        lessonEvent.setLesson(lessonDao.findByParameters(
                userDao.findUserByUsername(principal.getUsername()).getId(),
                lessonEventDto.getClassId(),
                lessonEventDto.getSubjectId())
        );
        lessonEvent.setComment(lessonEventDto.getComment());
        if (lessonEventDto.getEventDate() != null) {
            lessonEvent.setEventDate(LocalDate.parse(lessonEventDto.getEventDate()));
        }
        if (lessonEventDto.getLessonEventTypeId() != null){
        lessonEvent.setLessonEventType(lessonEventTypeDao.findOne(lessonEventDto.getLessonEventTypeId()));
        }
        lessonEventDao.create(lessonEvent);
        return lessonEvent.getId();
    }

    @Transactional
    @Override
    public ChildEvent getChildEvent(LessonEventDto lessonEventDto) {
        ChildEvent childEvent = new ChildEvent();
        childEvent.setLessonEventId(createLessonEvent(lessonEventDto, UserDetailsService.getPrincipal()));
        childEvent.setChildDtos(getChild(lessonEventDto.getClassId()));
        return childEvent;
    }

    @Transactional
    @Override
    public JournalDto createChildMark(JournalDto journalDto) {
        LessonEvent lessonEvent = lessonEventDao.findOne(journalDto.getLessonEventId());
        ArrayList<ChildMark> childMarks = new ArrayList<>();
        for (ChildMarkDto child : journalDto.getChildMarkDtos()) {
            childMarks.add(new ChildMark(lessonEvent,
                    childDao.findOne(child.getChildId()),
                    child.getMark(),
                    journalDto.getAbsents().contains(child.getChildId())));
        }
        lessonEvent.setChildMarkList(childMarks);
        lessonEvent.setComment(journalDto.getComment());
        lessonEvent.setCompleted(journalDto.isCompleted());
        if (journalDto.isCompleted()){
            lessonEvent.setCompletionDate(LocalDate.now());
        }
        lessonEventDao.update(lessonEvent);
        return journalDto;
    }

    @Transactional
    @Override
    public List<LessonEvent> getEventsByChildIdAndDate(String childId, LocalDate date) {
        return lessonEventDao.findLessonEventsByChildIdAndDate(childId, date);
    }

    private boolean isContains(List<LessonDto> lessonDtos, LessonDto lessonDto) {
        for (LessonDto lesson : lessonDtos) {
            if (lesson.getSubject().getId().equals(lessonDto.getSubject().getId())) {
                return false;
            }
        }
        return true;
    }

}
