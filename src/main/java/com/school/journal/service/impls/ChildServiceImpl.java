package com.school.journal.service.impls;

import com.school.journal.dao.interfaces.AuthorityDao;
import com.school.journal.dao.interfaces.ChildDao;
import com.school.journal.dto.NewChildDto;
import com.school.journal.mapper.ChildMapper;
import com.school.journal.model.security.Authority;
import com.school.journal.model.users.Child;
import com.school.journal.service.interfaces.ChildService;
import com.school.journal.service.interfaces.SchoolClassService;
import com.school.journal.service.interfaces.UserService;
import com.school.journal.utils.PasswordGenerator;
import com.school.journal.validation.UserValidation;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class ChildServiceImpl implements ChildService {

    @Value("${security.password.length}")
    private int passwordLength;

    @Autowired
    private UserService userService;

    @Autowired
    private ChildDao childDao;

    @Autowired
    private SchoolClassService schoolClassService;

    @Autowired
    private AuthorityDao authorityDao;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Transactional
    public String create(Child child) {
        String generatedPassword = prepare(child);

        if (!UserValidation.checkChild(child)) {
            return null;
        }

        childDao.create(child);
        return generatedPassword;
    }

    @Transactional
    public void update(NewChildDto childDto) {
        Child newChild = ChildMapper.MAPPER.fromDto(childDto);
        Child child = getById(childDto.getId());

        if (!UserValidation.checkChild(child)) {
            return;
        }

        userService.updatePersonalInfo(child, newChild);
        child.setSchoolClass(schoolClassService.getById(childDto.getSchoolClassId()));

        childDao.update(child);
    }

    public Child getById(String id) {
        Child child = childDao.findOne(id);
        Hibernate.initialize(child.getParents());
        return child;
    }

    @Transactional
    public void deactivate(Child child) {
        child.setActive(false);
        childDao.update(child);
    }

    public List<Child> getAll() {
        return childDao.findAll();
    }

    public List<Child> getAllActive() { return childDao.getActiveChildren(); }

    public String prepare(Child child) {
        PasswordGenerator passwordGenerator = new PasswordGenerator.PasswordGeneratorBuilder()
                .useDigits(true).useLower(true).useUpper(true).build();

        child.setAuthorities(Collections.singletonList(authorityDao.findOne(Authority.AuthorityType.CHILD)));
        String password = passwordGenerator.generate(passwordLength);
        child.setPassword(passwordEncoder.encode(password)); child.setActive(true);
        return password;
    }
}
