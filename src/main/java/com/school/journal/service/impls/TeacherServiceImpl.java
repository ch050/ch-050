package com.school.journal.service.impls;

import com.school.journal.dao.interfaces.AuthorityDao;
import com.school.journal.dao.interfaces.TeacherDao;
import com.school.journal.dto.SubjectDto;
import com.school.journal.dto.TeacherDto;
import com.school.journal.model.security.Authority;
import com.school.journal.model.users.Teacher;
import com.school.journal.service.interfaces.TeacherService;
import com.school.journal.utils.PasswordGenerator;
import com.school.journal.validation.UserValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
public class TeacherServiceImpl implements TeacherService {

    @Autowired
    private TeacherDao teacherDao;

    @Autowired
    private AuthorityDao authorityDao;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public List<TeacherDto> getTeacher() {
        return teacherDao
                .findAll()
                .stream()
                .filter(Teacher :: isActive)
                .map(TeacherDto::new)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public void deleteTeacher(String teacherId) {
        Teacher teacher = teacherDao.findOne(teacherId);
        teacher.setActive(false);
        teacherDao.update(teacher);
    }

    @Transactional
    @Override
    public Teacher createTeacher(TeacherDto teacherDto) {
        List<Authority> authorities = new ArrayList<>();
        authorities.add(authorityDao.findOne(Authority.AuthorityType.TEACHER));
        PasswordGenerator passwordGenerator = new PasswordGenerator.PasswordGeneratorBuilder()
                .useDigits(true).useLower(true).useUpper(true).build();
        Teacher teacher = new Teacher(teacherDto.getId(), teacherDto.getNickname(),
               passwordEncoder.encode(passwordGenerator.generate(10)),
                true, teacherDto.getFirstName(),
                teacherDto.getPatronymic(), teacherDto.getLastName(),
                teacherDto.getPhoneNumber(), authorities,
                teacherDto.getDescription(), teacherDto.getEmail());
        if (!UserValidation.checkTeacher(teacher)) {
            return null;
        }
        teacherDao.create(teacher);
        return teacher;
    }

    @Transactional
    @Override
    public void updateTeacher(TeacherDto teacherDto) {
        teacherDao.update(new Teacher(teacherDto.getId(), teacherDto.getNickname(),
                teacherDao.findOne(teacherDto.getId()).getPassword(), true,
                teacherDto.getFirstName(), teacherDto.getPatronymic(),
                teacherDto.getLastName(), teacherDto.getPhoneNumber(),
                teacherDao.findOne(teacherDto.getId()).getAuthorities(),
                teacherDto.getDescription(),teacherDto.getEmail()));
    }

}
