package com.school.journal.controller;

import com.school.journal.model.security.Authority;
import com.school.journal.service.impls.UserDetailsService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@RequestMapping(value = "/")
public class HomePageController {

    @RequestMapping
    public String getHomePage() {
        Authority authority = (Authority) UserDetailsService
                .getAuthority()
                .get(0);
        return authority.getDefaultPage();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/login")
    public String login() {
        return "login";
    }

    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage() {
        SecurityContextHolder.getContext().getAuthentication().setAuthenticated(false);
        return "redirect:/login";
    }

}
