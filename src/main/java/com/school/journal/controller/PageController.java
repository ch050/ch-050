package com.school.journal.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PageController {

    @GetMapping(value = "/languageTest")
    public String lesson() {
        return "languageTest/test";
    }

    @GetMapping("/children/all")
    public String allChildren() {
        return "children/children";
    }

    @GetMapping("/children/addChild")
    public String addChild() {
        return "children/addChild";
    }

    @GetMapping("/children/editChild")
    public String editChild() {
        return "children/editChild";
    }

    @GetMapping("/parents/all")
    public String allParents() {
        return "parents/parents";
    }

    @GetMapping("/parents/editParent")
    public String editParent() {
        return "parents/editParent";
    }

    @GetMapping("/classes/all")
    public String allClasses() {
        return "schoolClasses/schoolClasses";
    }

    @GetMapping("/classes/addClass")
    public String addClass() {
        return "schoolClasses/addSchoolClass";
    }

    @GetMapping("/subjects")
    public String subject() {
        return "subject/subject";
    }

    @GetMapping("/teachers")
    public String teacher() {
        return "teacherPage/teacher";
    }

    @GetMapping("/teacherJournal")
    public String teacherJournal() {
        return "teacherJournal/teacherJournal";
    }

    @GetMapping("/lesson")
    public String lessonPage() {
        return "lessonPage/lessons";
    }

    @GetMapping("/journal")
    public String journal() {
        return "teacher/lesson";
    }

    @GetMapping("/parent/marks")
    public String childMarks() {
        return "parentView/parent_page";
    }

    @GetMapping("/parent/childModal")
    public String childModal() {
        return "parentView/modal";
    }

}