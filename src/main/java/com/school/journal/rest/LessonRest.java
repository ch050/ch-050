package com.school.journal.rest;

import com.school.journal.dto.*;
import com.school.journal.model.events.Lesson;
import com.school.journal.service.interfaces.LessonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class LessonRest {

    @Autowired
    private LessonService lessonService;

    @RequestMapping(method = RequestMethod.GET, value = "/lessons")
    public ResponseEntity<List<LessonPageDto>> getLessons() {
        return ResponseEntity.ok(lessonService.getAllLessons());
    }

    @RequestMapping(method = RequestMethod.GET, value = "/lessons/schoolClass")
    public  ResponseEntity<List<SchoolClassDto>> getSchoolClass(){
        return ResponseEntity.ok(lessonService.getSchoolClass());
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/lessons/{id}")
    public ResponseEntity<Boolean> deleteLesson(@PathVariable(name = "id") String lessonId) {
         lessonService.deleteLessons(lessonId);
         return ResponseEntity.ok(true);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/lessons")
    public ResponseEntity<Lesson> createLesson(@RequestBody LessonPageDto lessonDto){
        return ResponseEntity.ok(lessonService.createLesson(lessonDto));
    }


    @RequestMapping(method = RequestMethod.PUT, value = "/lessons")
    public ResponseEntity<LessonPageDto> updateLesson(@RequestBody LessonPageDto lessonDto){
        lessonService.updateLesson(lessonDto);
        return ResponseEntity.ok(lessonDto);
    }


}
