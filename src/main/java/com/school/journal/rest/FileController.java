package com.school.journal.rest;

import com.school.journal.dto.FilesDto;
import com.school.journal.model.events.FileResource;
import com.school.journal.service.interfaces.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.nio.file.Paths;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_PDF_VALUE;

@RestController
@RequestMapping("/files")
public class FileController {

    @Value("${upload.file_accept}")
    private String fileAccept;

    @Value("${upload.max_upload_size}")
    private String maxUploadSize;

    @Autowired
    private FileService fileService;

    @RequestMapping(value = "/download/{fileId}", method = RequestMethod.GET, produces = "application/pdf;charset=UTF-8")
    public void download(@PathVariable("fileId") String fileId, HttpServletResponse response) throws IOException {
        FileResource fileResource = fileService.getFileById(fileId);
        File file = Paths.get(fileService.getUploadRootPath(), fileResource.getRelativePath()).toFile();

        InputStream in = new FileInputStream(file);
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Content-Disposition", "attachment; filename=" + encodeToUTF8(fileResource.getName()));
        response.setHeader("Content-Length", String.valueOf(file.length()));
        FileCopyUtils.copy(in, response.getOutputStream());
    }

    @RequestMapping(value = "/updateFiles", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
    public String updateFiles(
            @RequestParam(value = "addedFiles", required = false) MultipartFile[] addedFiles,
            @RequestParam(value = "filenames", required = false) String[] filenames,
            @RequestParam(value = "filesForRename", required = false) String[] filesIdForRename,
            @RequestParam(value = "newNames", required = false) String[] newNames,
            @RequestParam(value = "filesForRemove", required = false) String[] filesIdForRemove,
            @RequestParam(value = "subject", required = false) String subject,
            @RequestParam(value = "eventId", required = false) String eventId) throws IOException {

        saveFiles(addedFiles, filenames, subject, eventId);
        renameFiles(filesIdForRename, newNames);
        removeFiles(filesIdForRemove);

        return null;
    }

    @RequestMapping(value = "/getFiles/{eventId}", method = RequestMethod.GET)
    public FilesDto getUploadedFilesByEventId(@PathVariable("eventId") String eventId) {
        List<FileResource> fileResources = fileService.getFilesByEventId(eventId);
        return new FilesDto(fileAccept, maxUploadSize, fileResources);
    }

    private void saveFiles(MultipartFile[] addedFiles, String[] filenames, String subject, String eventId) throws IOException {
        if (addedFiles != null && filenames != null && addedFiles.length == filenames.length) {
            for (int i = 0; i < addedFiles.length; i++) {
                if (FileService.isFileExtensionAllowed(filenames[i], fileAccept)) {
                    fileService.saveFile(subject, eventId, addedFiles[i].getBytes(), filenames[i]);
                }
            }
        }
    }

    private void renameFiles(String[] filesIdForRename, String[] newNames) {
        if (filesIdForRename != null && newNames != null && filesIdForRename.length == newNames.length) {
            for (int i = 0; i < filesIdForRename.length; i++) {
                fileService.rename(filesIdForRename[i], newNames[i]);
            }
        }
    }

    private void removeFiles(String[] filesIdForRemove) {
        if (filesIdForRemove != null) {
            for (String fileId : filesIdForRemove) {
                fileService.removeFileById(fileId);
            }
        }
    }

    private String encodeToUTF8(String string) throws UnsupportedEncodingException {
        return URLEncoder.encode(string, "UTF-8").replaceAll("\\+", "%20");
    }

}