package com.school.journal.rest;

import com.school.journal.dto.ChildrenPageDto;
import com.school.journal.dto.ParentDto;
import com.school.journal.dto.RelationshipDto;
import com.school.journal.mapper.ChildMapper;
import com.school.journal.mapper.ParentMapper;
import com.school.journal.mapper.RelationshipMapper;
import com.school.journal.model.users.Child;
import com.school.journal.model.users.Parent;
import com.school.journal.model.users.SchoolClass;
import com.school.journal.service.interfaces.ChildService;
import com.school.journal.service.interfaces.ParentService;
import com.school.journal.service.interfaces.SchoolClassService;
import com.school.journal.validation.UserValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/children")
public class ChildrenRest {

    @Autowired
    private ChildService childService;

    @Autowired
    private ParentService parentService;

    @Autowired
    private SchoolClassService schoolClassService;

    @RequestMapping("/getAll")
    public ChildrenPageDto getAll() {
        List<SchoolClass> classes = schoolClassService.getAllActive();
        List<Child> children = childService.getAllActive();
        return new ChildrenPageDto(classes, children);
    }

    @RequestMapping(value = "/getChildAndParents", method = RequestMethod.POST, produces = "application/json")
    public RelationshipDto getChildById(@RequestParam String childId) {
        Child child = childService.getById(childId);
        return RelationshipMapper.MAPPER.toDto(child, child.getParents());
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = "application/json")
    public String addChild(@RequestBody RelationshipDto newRelationship) {
        Child child = ChildMapper.MAPPER.fromDto(newRelationship.getNewChild());
        child.setSchoolClass(schoolClassService.getById(newRelationship.getNewChild().getSchoolClassId()));

        List<ParentDto> parents = newRelationship.getParents();

        if (parents.isEmpty()) {
            return null;
        }

        String password = childService.create(child);

        for (ParentDto parentDto : parents) {
            Parent parent = ParentMapper.MAPPER.fromDto(parentDto);
            if (!UserValidation.checkParent(parent)) {
                return null;
            }
            parent.setChildren(Collections.singletonList(child));
            parentService.create(parent);
        }

        return password;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST, produces = "application/json")
    public String editChild(@RequestBody RelationshipDto relationship) {

        List<ParentDto> parents = relationship.getParents();
        if (parents.isEmpty()) {
            return null;
        }

        childService.update(relationship.getNewChild());

        for (ParentDto parentDto : parents) {
            parentService.update(parentDto);
        }
        return null;
    }

    @RequestMapping(value = "/deactivate", method = RequestMethod.POST, produces = "application/json")
    public String deactivate(@RequestParam("childId") String childId) {
        childService.deactivate(childService.getById(childId));
        return null;
    }

}
