package com.school.journal.rest;

import com.school.journal.components.interfaces.PagesManager;
import com.school.journal.components.interfaces.ResourceBundleManager;
import com.school.journal.dao.interfaces.UserDao;
import com.school.journal.dto.HeaderDto;
import com.school.journal.model.security.User;
import com.school.journal.service.impls.UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/header")
public class HeaderController {

    @Value("${msg.default_language}")
    private String defaultLanguage;

    @Value("${msg.pages}")
    private List<String> pages;

    @Autowired
    private ResourceBundleManager resourceBundleManager;

    @Autowired
    private PagesManager pagesManager;

    @Autowired
    private UserDao userDao;

    @RequestMapping(value = "/loadLanguage/{language}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Map<String, String> load(
            @PathVariable("language") String language,
            @RequestParam(value = "page") String page) throws Exception {
        return resourceBundleManager.getWords(page, new Locale(language));
    }

    @RequestMapping(value = "/getData", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public HeaderDto getHeaderInfo(
            @RequestParam("page") String page,
            @RequestParam(value = "language", required = false) String language) {

        org.springframework.security.core.userdetails.User principal = UserDetailsService.getPrincipal();
        User user = userDao.findUserByUsername(principal.getUsername());

        language = language == null ? defaultLanguage : language;

        HeaderDto headerDto = new HeaderDto();
        headerDto.setNickname(user.getNickname());
        headerDto.setEmail(user.getEmail());
        headerDto.setPages(pagesManager.getAllowedPages(principal.getAuthorities()));
        headerDto.setLanguage(language);
        headerDto.setMainPage(pagesManager.getMainPage(principal.getAuthorities()));
        headerDto.setMessages(resourceBundleManager.getWords(page, new Locale(language)));

        return headerDto;
    }



}