package com.school.journal.rest;

import com.school.journal.dto.SubjectDto;
import com.school.journal.dto.TeacherDto;
import com.school.journal.model.users.Teacher;
import com.school.journal.service.interfaces.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TeacherRest {

    @Autowired
    private TeacherService teacherService;

    @RequestMapping(method = RequestMethod.GET, value = "/teacher")
    public ResponseEntity<List<TeacherDto>> getTeachers(){
        return ResponseEntity.ok(teacherService.getTeacher());
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/teachers/{id}")
    public ResponseEntity<Boolean> deleteTeacher(@PathVariable(name = "id") String teacherId) {
        teacherService.deleteTeacher(teacherId);
        return ResponseEntity.ok(true);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/teachers")
    public ResponseEntity<Teacher> createTeacher(@RequestBody TeacherDto teacherDto) {
        return ResponseEntity.ok(teacherService.createTeacher(teacherDto));
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/teachers")
    public ResponseEntity<TeacherDto> updateTeacher(@RequestBody TeacherDto teacherDto) {
        teacherService.updateTeacher(teacherDto);
        return ResponseEntity.ok(teacherDto);
    }

}
