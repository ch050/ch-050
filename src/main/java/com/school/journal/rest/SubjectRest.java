package com.school.journal.rest;

import com.school.journal.dto.SubjectDto;
import com.school.journal.model.events.Subject;
import com.school.journal.service.interfaces.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SubjectRest {

    @Autowired
    private SubjectService subjectService;

    @RequestMapping(method = RequestMethod.GET, value = "/subject")
    public ResponseEntity<List<SubjectDto>> getSubjects() {
        return ResponseEntity.ok(subjectService.getSubjects());
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/subjects/{id}")
    public ResponseEntity<Boolean> deleteSubject(@PathVariable(name = "id") String subjectId) {
        subjectService.deleteSubject(subjectId);
        return ResponseEntity.ok(true);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/subjects")
    public ResponseEntity<Subject> createSubject(@RequestBody SubjectDto subjectDto) {
        return ResponseEntity.ok(subjectService.createSubject(subjectDto));
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/subjects")
    public ResponseEntity<SubjectDto> updateSubject(@RequestBody SubjectDto subjectDto) {
        subjectService.updateSubject(subjectDto);
        return ResponseEntity.ok(subjectDto);
    }

}
