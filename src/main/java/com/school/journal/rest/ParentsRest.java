package com.school.journal.rest;

import com.school.journal.dto.ParentDto;
import com.school.journal.dto.ParentsPageDto;
import com.school.journal.mapper.ParentMapper;
import com.school.journal.model.users.Parent;
import com.school.journal.service.interfaces.ParentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/parents")
public class ParentsRest {

    @Autowired
    private ParentService parentService;

    @RequestMapping("/getAll")
    public ParentsPageDto getAll() {
        List<Parent> parents = parentService.getAllActive();
        return new ParentsPageDto(parents);
    }

    @RequestMapping(value = "/get", method = RequestMethod.POST, produces = "application/json")
    public ParentDto getParent(@RequestParam(value = "parentId") String parentId) {
        return ParentMapper.MAPPER.toDto(parentService.getById(parentId));
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST, produces = "application/json")
    public String editParent(@RequestBody ParentDto parentDto) {
        parentService.update(parentDto);
        return null;
    }

    @RequestMapping(value = "/deactivate", method = RequestMethod.POST, produces = "application/json")
    public String deactivate(@RequestParam("parentId") String parentId) {
        parentService.deactivate(parentService.getById(parentId));
        return null;
    }

}
