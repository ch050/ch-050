package com.school.journal.rest;

import com.school.journal.dto.NewSchoolClassDto;
import com.school.journal.dto.SchoolClassesPageDto;
import com.school.journal.model.users.SchoolClass;
import com.school.journal.service.interfaces.SchoolClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/classes")
public class SchoolClassesRest {

    @Autowired
    private SchoolClassService schoolClassService;

    @RequestMapping("/getAll")
    public SchoolClassesPageDto getAll() {
        List<SchoolClass> classes = schoolClassService.getAllActive();
        return new SchoolClassesPageDto(classes);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = "application/json")
    public String addClass(@ModelAttribute NewSchoolClassDto schoolClassDto) {
        schoolClassService.add(schoolClassDto);
        return "";
    }

    @RequestMapping(value = "/deactivate", method = RequestMethod.POST, produces = "application/json")
    public String deactivate(@RequestParam("schoolClassId") String schoolClassId) {
        schoolClassService.deactivate(schoolClassService.getById(schoolClassId));
        return null;
    }

}
