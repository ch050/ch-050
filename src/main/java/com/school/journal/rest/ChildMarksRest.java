package com.school.journal.rest;

import com.school.journal.dto.*;
import com.school.journal.mapper.ChildEventsMapper;
import com.school.journal.model.events.LessonEvent;
import com.school.journal.service.interfaces.ChildMarksService;
import com.school.journal.service.interfaces.LessonEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/parent")
public class ChildMarksRest {

    @Autowired
    private ChildMarksService childMarksService;

    @Autowired
    private LessonEventService lessonEventService;

    @Value("${date_separator}")
    private String dateSeparator;

    @RequestMapping(value = "/getChildEvents", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<ChildEventDto> getChildEvents(
            @RequestParam(value = "childId", required = false) String childId,
            @RequestParam(value = "date", required = false) String eventDate) {

        String[] dateParts = eventDate.split(dateSeparator);
        if (dateParts.length != 3) throw new IllegalArgumentException("Wrong date format");

        int year = Integer.parseInt(dateParts[0]);
        int month = Integer.parseInt(dateParts[1]);
        int day = Integer.parseInt(dateParts[2]);

        List<LessonEvent> eventsByChildIdAndDate = lessonEventService.getEventsByChildIdAndDate(childId, LocalDate.of(year, month, day));
        return ChildEventsMapper.toDto(eventsByChildIdAndDate);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/children/info")
    public ResponseEntity<ParentPageDto> getInfo(){
        return ResponseEntity.ok(childMarksService.getInfo());
    }
}
