package com.school.journal.rest;

import com.school.journal.dto.TeacherJournalDto;
import com.school.journal.dto.TeacherLessonEventDto;
import com.school.journal.service.interfaces.TeacherJournalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TeacherJournalRest {

    @Autowired
    private TeacherJournalService teacherJournalService;

    @RequestMapping(method = RequestMethod.POST, value = "/teachersJournal")
    public TeacherJournalDto getJournal(@RequestBody TeacherLessonEventDto lessonEventDto){
       return teacherJournalService.getJournal(lessonEventDto);
    }

}
