package com.school.journal.rest;

import com.school.journal.dto.ChildEvent;
import com.school.journal.dto.JournalDto;
import com.school.journal.dto.LessonDto;
import com.school.journal.dto.LessonEventDto;
import com.school.journal.service.impls.UserDetailsService;
import com.school.journal.service.interfaces.LessonEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/lessonEvent")
class LessonEventRest {

    @Autowired
    private LessonEventService lessonEventService;

    @RequestMapping(method = RequestMethod.GET, value = "/lesson")
    public ResponseEntity<List<LessonDto>> getLessons() {
        return ResponseEntity.ok(lessonEventService.getTeachersLessons(UserDetailsService.getPrincipal()));
    }

    @RequestMapping(method = RequestMethod.POST, value = "/createLessonEvent", produces = "application/json")
    public ResponseEntity<ChildEvent> getLessonSubject(@RequestBody LessonEventDto lessonEventDto) {
        return ResponseEntity.ok(lessonEventService.getChildEvent(lessonEventDto));
    }

    @RequestMapping(method = RequestMethod.POST, value = "/create")
    public ResponseEntity<JournalDto> createLessonEvent(@RequestBody JournalDto journalDto) {
        return ResponseEntity.ok(lessonEventService.createChildMark(journalDto));
    }

}
