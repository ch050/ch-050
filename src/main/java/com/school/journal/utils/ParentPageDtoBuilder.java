package com.school.journal.utils;

import com.school.journal.dto.ParentPageDto;
import com.school.journal.model.events.ChildMark;
import com.school.journal.dto.subsidiary.ChildInfo;
import com.school.journal.dto.subsidiary.MarksByDate;
import com.school.journal.model.users.Child;
import com.school.journal.model.users.Parent;

import java.util.*;

public class ParentPageDtoBuilder {

    private final Parent parent;
    private final List<ChildMark> childMarks;

    private final Map<String, ChildInfo> childrenInfo = new HashMap<>();

    public ParentPageDtoBuilder(Parent parent, List<ChildMark> childMarks) {
        this.parent = parent;
        this.childMarks = childMarks;
    }

    public ParentPageDto build() {

        for (ChildMark childMark : this.childMarks){
            final Child child = childMark.getChild();

            if (!this.childrenInfo.containsKey(child.getId())){
                this.childrenInfo.put(child.getId(), buildChildInfo(childMark, child));
                continue;
            }

            updateChild(childMark, child);
        }

        return new ParentPageDto(parent.getNickname(), parent.getEmail(), new ArrayList<>(this.childrenInfo.values()));
    }

    private ChildInfo buildChildInfo(ChildMark childMark, Child child) {
        final ChildInfo childInfo = new ChildInfo();

        childInfo.setChildId(child.getId());
        childInfo.setChildName(child.getFirstName());
        childInfo.setSubjects(getSingleValueSubjects(childMark));
        childInfo.setMarks(getSingleValuedMarksByDatesList(getMarksByDate(childMark)));

        return childInfo;
    }

    private Set<String> getSingleValueSubjects(ChildMark childMark) {
        final Set<String> subjects = new LinkedHashSet<>();
        subjects.add(childMark.getLessonEvent().getLesson().getSubject().getName());
        return subjects;
    }

    private List<MarksByDate> getSingleValuedMarksByDatesList(MarksByDate mbd) {
        final List<MarksByDate> marks = new ArrayList<>();
        marks.add(mbd);
        return marks;
    }

    private void updateChild(ChildMark childMark, Child child) {
        final ChildInfo childInfo = this.childrenInfo.get(child.getId());
        childInfo.addSubject(childMark.getLessonEvent().getLesson().getSubject().getName());

        final String eventDate = String.valueOf(childMark.getLessonEvent().getEventDate());

        if(childInfo.hasMarksByDate(eventDate)) {
            childInfo.getMarksByDate(eventDate).addMark(childMark.getStringMark());
            return;
        }

        childInfo.addMarkByDate(new MarksByDate(eventDate, new String[] {childMark.getStringMark()}));
    }

    private MarksByDate getMarksByDate(ChildMark childMark) {
        final MarksByDate mbd = new MarksByDate(String.valueOf(childMark.getLessonEvent().getEventDate()));

        mbd.addMark(childMark.getStringMark());

        return mbd;
    }

}
