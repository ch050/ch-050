package com.school.journal.configuration;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;

@Configuration
@Import({
        WebSecurityConfiguration.class,
        HibernateConfiguration.class
})
@ComponentScan(basePackages = {
        "com.school.journal.service"
})
public class SchoolJournalConfiguration {

    @Value("${journal.env:local}")
    private static String environment;

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        PropertySourcesPlaceholderConfigurer configurer = new PropertySourcesPlaceholderConfigurer();
        configurer.setLocations(
                new ClassPathResource("default.properties"),
                new ClassPathResource(environment + ".properties"),
                new FileSystemResource("/etc/journal/app.properties")
        );
        configurer.setIgnoreResourceNotFound(true);
        return configurer;
    }

}
