package com.school.journal.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.codec.Hex;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

import java.security.SecureRandom;

import static com.school.journal.model.security.Authority.AuthorityType.*;

@EnableWebSecurity
@ComponentScan(basePackages = "com.school.journal")
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Value("${security.passkey.secret}")
    private CharSequence securityKey;

    @Value("${security.passkey.strength}")
    private int strength;

    @Autowired
    private AuthenticationSuccessHandler authenticationSuccessHandler;

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(strength, new SecureRandom(Hex.decode(securityKey)));
    }

    @Bean
    public DaoAuthenticationProvider daoAuthenticationProvider(Object passwordEncoder, org.springframework.security.core.userdetails.UserDetailsService userDetailsService) {
        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setPasswordEncoder(passwordEncoder);
        daoAuthenticationProvider.setUserDetailsService(userDetailsService);
        return daoAuthenticationProvider;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/ui/**", "/login", "/logout").permitAll()
                .antMatchers("/children/**").hasAnyAuthority(ADMIN.toString(), TEACHER.toString())
                .antMatchers("/parents/**", "/classes/**", "/subjects", "/teachers", "/lessons", "/journal").hasAuthority(ADMIN.toString())
                .antMatchers("/journal", "/teacherJournal").hasAuthority(TEACHER.toString())
                .antMatchers("/parent/marks").hasAuthority(PARENT.toString())
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .successHandler(authenticationSuccessHandler)
                .and()
                .csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());
    }

}