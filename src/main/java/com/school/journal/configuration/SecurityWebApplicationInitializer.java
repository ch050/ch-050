package com.school.journal.configuration;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * We need this class to initialize spring web security.
 * Without this empty class application security will bi disable
 */
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {

}
