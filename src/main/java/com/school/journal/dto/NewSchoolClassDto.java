package com.school.journal.dto;

public class NewSchoolClassDto {

    private String name;
    private String curatorId;

    public NewSchoolClassDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCuratorId() {
        return curatorId;
    }

    public void setCuratorId(String curatorId) {
        this.curatorId = curatorId;
    }
}
