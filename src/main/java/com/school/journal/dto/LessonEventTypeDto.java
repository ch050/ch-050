package com.school.journal.dto;

import com.school.journal.model.events.LessonEventType;

public class LessonEventTypeDto {

    private String id;

    private String name;

    public LessonEventTypeDto(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public LessonEventTypeDto(LessonEventType lessonEventType) {
        this.id = lessonEventType.getId();
        this.name = lessonEventType.getName();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
