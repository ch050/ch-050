package com.school.journal.dto;

public class SchoolClassRowDto {

    private String id;
    private String name;
    private String curatorId;
    private String curatorLastName;
    private String curatorFirstName;
    private String curatorPatronymic;
    private String startDate;
    private String endDate;

    public SchoolClassRowDto() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCuratorId() {
        return curatorId;
    }

    public void setCuratorId(String curatorId) {
        this.curatorId = curatorId;
    }

    public String getCuratorLastName() {
        return curatorLastName;
    }

    public void setCuratorLastName(String curatorLastName) {
        this.curatorLastName = curatorLastName;
    }

    public String getCuratorFirstName() {
        return curatorFirstName;
    }

    public void setCuratorFirstName(String curatorFirstName) {
        this.curatorFirstName = curatorFirstName;
    }

    public String getCuratorPatronymic() {
        return curatorPatronymic;
    }

    public void setCuratorPatronymic(String curatorPatronymic) {
        this.curatorPatronymic = curatorPatronymic;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
