package com.school.journal.dto;

import com.school.journal.model.events.Lesson;
import com.school.journal.model.users.SchoolClass;
import com.school.journal.model.users.Teacher;

public class LessonPageDto {

    private String id;

    private TeacherDto teacher;

    private SubjectDto subject;

    private SchoolClassDto schoolClass;

    public LessonPageDto() {
    }

    public LessonPageDto(Lesson lesson){
        this.id = lesson.getId();
        this.teacher = new TeacherDto(lesson.getTeacher());
        this.subject = new SubjectDto(lesson.getSubject());
        this.schoolClass = new SchoolClassDto(lesson.getSchoolClass());
    }

    public LessonPageDto(String id, TeacherDto teacher, SubjectDto subject, SchoolClassDto schoolClass) {
        this.id = id;
        this.teacher = teacher;
        this.subject = subject;
        this.schoolClass = schoolClass;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public TeacherDto getTeacher() {
        return teacher;
    }

    public void setTeacher(TeacherDto teacher) {
        this.teacher = teacher;
    }

    public SubjectDto getSubject() {
        return subject;
    }

    public void setSubject(SubjectDto subject) {
        this.subject = subject;
    }

    public SchoolClassDto getSchoolClass() {
        return schoolClass;
    }

    public void setSchoolClass(SchoolClassDto schoolClass) {
        this.schoolClass = schoolClass;
    }

}
