package com.school.journal.dto;

import com.school.journal.model.events.FileResource;
import org.hibernate.Hibernate;

import java.util.ArrayList;
import java.util.List;

public class FilesDto {

    private String fileAccept;
    private String maxUploadSize;
    private List<FileDto> files;

    public FilesDto() {}

    public FilesDto(String fileAccept, String maxUploadSize, List<FileResource> fileResources) {
        this.fileAccept = fileAccept;
        this.maxUploadSize = maxUploadSize;
        this.files = new ArrayList<>(fileResources.size());
        fileResources.forEach(file -> this.files.add(new FileDto(file)));
    }

    public String getFileAccept() {
        return fileAccept;
    }
    public void setFileAccept(String fileAccept) {
        this.fileAccept = fileAccept;
    }

    public String getMaxUploadSize() {
        return maxUploadSize;
    }
    public void setMaxUploadSize(String maxUploadSize) {
        this.maxUploadSize = maxUploadSize;
    }

    public List<FileDto> getFiles() {
        return files;
    }
    public void setFiles(List<FileDto> files) {
        this.files = files;
    }

    private class FileDto {
        private String id;
        private String name;

        public FileDto() {}

        public FileDto(FileResource fileResource) {
            this.id = fileResource.getId();
            this.name = fileResource.getName();
        }

        public String getId() {
            return id;
        }
        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }
    }

}
