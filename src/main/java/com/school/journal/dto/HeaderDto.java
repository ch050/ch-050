package com.school.journal.dto;

import java.util.Map;

public class HeaderDto {

    private String nickname;
    private String email;
    private String language;
    private String mainPage;
    private Map<String, String> pages;
    private Map<String, String> messages;

    public HeaderDto() {}

    public String getNickname() {
        return nickname;
    }
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getLanguage() {
        return language;
    }
    public void setLanguage(String language) {
        this.language = language;
    }

    public String getMainPage() {
        return mainPage;
    }
    public void setMainPage(String mainPage) {
        this.mainPage = mainPage;
    }

    public Map<String, String> getPages() {
        return pages;
    }
    public void setPages(Map<String, String> pages) {
        this.pages = pages;
    }

    public Map<String, String> getMessages() {
        return messages;
    }
    public void setMessages(Map<String, String> messages) {
        this.messages = messages;
    }

}