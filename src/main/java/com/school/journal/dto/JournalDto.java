package com.school.journal.dto;

import java.util.List;

public class JournalDto {

    private String comment;

    private boolean completed;

    private String lessonEventId;

    private List<String> absents;

    private List<ChildMarkDto> childMarkDtos;

    public JournalDto() {
    }

    public JournalDto(String comment, boolean completed, String lessonEventId, List<String> absents, List<ChildMarkDto> childMarkDtos) {
        this.comment = comment;
        this.completed = completed;
        this.lessonEventId = lessonEventId;
        this.absents = absents;
        this.childMarkDtos = childMarkDtos;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getLessonEventId() {
        return lessonEventId;
    }

    public void setLessonEventId(String lessonEventId) {
        this.lessonEventId = lessonEventId;
    }

    public List<String> getAbsents() {
        return absents;
    }

    public void setAbsents(List<String> absents) {
        this.absents = absents;
    }

    public List<ChildMarkDto> getChildMarkDtos() {
        return childMarkDtos;
    }

    public void setChildMarkDtos(List<ChildMarkDto> childMarkDtos) {
        this.childMarkDtos = childMarkDtos;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

}
