package com.school.journal.dto;

public class ChildMarkDto {

    private String childId;

    private Integer mark;

    public ChildMarkDto( String childId, Integer mark) {
        this.childId = childId;
        this.mark = mark;
    }

    public ChildMarkDto() {
    }

    public String getChildId() {
        return childId;
    }

    public void setChildId(String childId) {
        this.childId = childId;
    }

    public Integer getMark() {
        return mark;
    }

    public void setMark(Integer mark) {
        this.mark = mark;
    }

}
