package com.school.journal.dto;

import java.time.LocalDate;

public class TeacherEventJournalDto {

    private String typeName;

    private LocalDate eventDate;

    private String firstName;

    private String lastName;

    private String patronymic;

    private Boolean absent;

    private String mark;

    private String childId;

    public TeacherEventJournalDto(String typeName, LocalDate eventDate, String firstName, String lastName, String patronymic, Boolean absent, String mark, String childId) {
        this.typeName = typeName;
        this.eventDate = eventDate;
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.absent = absent;
        this.mark = mark;
        this.childId = childId;
    }

    public TeacherEventJournalDto() {
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getChildId() {
        return childId;
    }

    public void setChildId(String childId) {
        this.childId = childId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public LocalDate getEventDate() {
        return eventDate;
    }

    public void setEventDate(LocalDate eventDate) {
        this.eventDate = eventDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public Boolean getAbsent() {
        return absent;
    }

    public void setAbsent(Boolean absent) {
        this.absent = absent;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

}
