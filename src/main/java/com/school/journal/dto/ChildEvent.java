package com.school.journal.dto;

import java.util.List;

public class ChildEvent {

    private List<ChildDto> childDtos;

    private String lessonEventId;

    public ChildEvent(List<ChildDto> childDtos, String lessonEventId) {
        this.childDtos = childDtos;
        this.lessonEventId = lessonEventId;
    }

    public ChildEvent() {
    }

    public List<ChildDto> getChildDtos() {
        return childDtos;
    }

    public void setChildDtos(List<ChildDto> childDtos) {
        this.childDtos = childDtos;
    }

    public String getLessonEventId() {
        return lessonEventId;
    }

    public void setLessonEventId(String lessonEventId) {
        this.lessonEventId = lessonEventId;
    }

}
