package com.school.journal.dto;

import com.school.journal.mapper.ParentRowMapper;
import com.school.journal.model.users.Parent;

import java.util.ArrayList;
import java.util.List;

public class ParentsPageDto {

    private List<ParentRowDto> parents = new ArrayList<>();

    public ParentsPageDto() {
    }

    public ParentsPageDto(List<Parent> parents) {
        parents.forEach(parent -> {
            this.parents.add(ParentRowMapper.MAPPER.toDto(parent));
        });
    }

    public List<ParentRowDto> getParents() {
        return parents;
    }

    public void setParents(List<ParentRowDto> parents) {
        this.parents = parents;
    }

}
