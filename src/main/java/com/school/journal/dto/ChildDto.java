package com.school.journal.dto;


import com.school.journal.model.users.Child;

public class ChildDto {

    private String id;

    private String firstName;

    private String patronymic;

    private String lastName;

    public ChildDto(Child child) {
        this.id = child.getId();
        this.firstName = child.getFirstName();
        this.patronymic = child.getPatronymic();
        this.lastName = child.getLastName();
    }

    public ChildDto() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

}
