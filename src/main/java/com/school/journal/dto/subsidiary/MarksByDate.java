package com.school.journal.dto.subsidiary;

import java.util.ArrayList;
import java.util.List;

public class MarksByDate {

    private String date;
    private List<String> marks = new ArrayList<>();

    public MarksByDate(String date, String ... marks) {
        this.date = date;

        for (String mark : marks) {
            this.marks.add(mark);
        }
    }

    public String getDate() {
        return date;
    }

    public void addMark(String mark) {
        this.marks.add(mark);
    }

    public List<String> getMarks() {
        return marks;
    }

}
