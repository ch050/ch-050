package com.school.journal.dto.subsidiary;

import java.util.List;
import java.util.Set;

public class ChildInfo {

    private String childId;
    private String childName;
    private Set<String> subjects;
    private List<MarksByDate> marks;

    public ChildInfo(){}

    public ChildInfo(String childId, String childName, Set<String> subjects, List<MarksByDate> marks) {
        this.childId = childId;
        this.childName = childName;
        this.subjects = subjects;
        this.marks = marks;
    }

    public String getChildId() {
        return childId;
    }

    public void setChildId(String childId) {
        this.childId = childId;
    }

    public String getChildName() {
        return childName;
    }

    public void setChildName(String childName) {
        this.childName = childName;
    }

    public Set<String> getSubjects() {
        return subjects;
    }

    public void setSubjects(Set<String> subjects) {
        this.subjects = subjects;
    }

    public List<MarksByDate> getMarks() {
        return marks;
    }

    public void setMarks(List<MarksByDate> marks) {
        this.marks = marks;
    }

    public void addSubject(String subject) {
        this.subjects.add(subject);
    }

    public void addMarkByDate(MarksByDate marksByDate) {
        this.marks.add(marksByDate);
    }

    public boolean hasMarksByDate(String date){
        for (MarksByDate item : marks){
            if (item.getDate().equals(date)) {
                return true;
            }
        }

        return false;
    }

    public MarksByDate getMarksByDate(String date){
        for (MarksByDate item : marks){
            if (item.getDate().equals(date)){
                return item;
            }
        }

        throw new IllegalArgumentException("No marks with specified date " + date);
    }

    @Override
    public String toString() {
        return "ChildInfo{" +
                "childId='" + childId + '\'' +
                ", childName='" + childName + '\'' +
                ", subjects=" + subjects +
                ", marks=" + marks +
                '}';
    }
}
