package com.school.journal.dto;

import com.school.journal.mapper.SchoolClassRowMapper;
import com.school.journal.model.users.SchoolClass;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SchoolClassesPageDto {

    private List<SchoolClassRowDto> schoolClasses = new ArrayList<>();

    public SchoolClassesPageDto() {
    }

    public SchoolClassesPageDto(List<SchoolClass> schoolClasses) {
        this.schoolClasses = schoolClasses
                .stream()
                .map(SchoolClassRowMapper.MAPPER::toDto)
                .collect(Collectors.toList());
    }

    public List<SchoolClassRowDto> getSchoolClasses() {
        return schoolClasses;
    }

    public void setSchoolClasses(List<SchoolClassRowDto> schoolClasses) {
        this.schoolClasses = schoolClasses;
    }
}
