package com.school.journal.dto;

import com.school.journal.dto.subsidiary.ChildInfo;

import java.util.List;

public class ParentPageDto {

    private String parentName;
    private String eMail;
    private List<ChildInfo> childrenData;

    public ParentPageDto(String parentName, String eMail, List<ChildInfo> childrenData) {
        this.parentName = parentName;
        this.eMail = eMail;
        this.childrenData = childrenData;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public List<ChildInfo> getChildrenData() {
        return childrenData;
    }

    public void setChildrenData(List<ChildInfo> childrenData) {
        this.childrenData = childrenData;
    }


}
