package com.school.journal.dto;

import com.school.journal.model.events.FileResource;

import java.util.List;

public class ChildEventDto {

    private String subject;
    private String comment;
    private List<FileResource> files;

    public ChildEventDto() {}

    public String getSubject() {
        return subject;
    }
    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getComment() {
        return comment;
    }
    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<FileResource> getFiles() {
        return files;
    }
    public void setFiles(List<FileResource> files) {
        this.files = files;
    }

}
