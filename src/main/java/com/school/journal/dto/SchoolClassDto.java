package com.school.journal.dto;

import com.school.journal.model.users.SchoolClass;

public class SchoolClassDto {

    private String id;

    private String name;

    public SchoolClassDto(SchoolClass schoolClass) {
        this.id = schoolClass.getId();
        this.name = schoolClass.getName();
    }

    public SchoolClassDto() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
