package com.school.journal.dto;

import java.util.List;

public class LessonDto {

    private List<SchoolClassDto> schoolClasses;

    private SubjectDto subject;

    private List<LessonEventTypeDto> typeDto;

    public LessonDto(List<SchoolClassDto> schoolClasses, SubjectDto subject, List<LessonEventTypeDto> typeDto) {
        this.schoolClasses = schoolClasses;
        this.subject = subject;
        this.typeDto = typeDto;
    }

    public LessonDto() {
    }

    public List<SchoolClassDto> getSchoolClasses() {
        return schoolClasses;
    }

    public void setSchoolClasses(List<SchoolClassDto> schoolClasses) {
        this.schoolClasses = schoolClasses;
    }

    public SubjectDto getSubject() {
        return subject;
    }

    public void setSubject(SubjectDto subject) {
        this.subject = subject;
    }

    public List<LessonEventTypeDto> getTypeDto() {
        return typeDto;
    }

    public void setTypeDto(List<LessonEventTypeDto> typeDto) {
        this.typeDto = typeDto;
    }

}