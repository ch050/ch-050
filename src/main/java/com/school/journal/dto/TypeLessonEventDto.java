package com.school.journal.dto;

import java.time.LocalDate;

public class TypeLessonEventDto {

    private String lessonEventType;

    private LocalDate eventDate;

    public TypeLessonEventDto() {
    }

    public TypeLessonEventDto(String lessonEventType, LocalDate eventDate) {
        this.lessonEventType = lessonEventType;
        this.eventDate = eventDate;
    }

    public String getLessonEventType() {
        return lessonEventType;
    }

    public void setLessonEventType(String lessonEventType) {
        this.lessonEventType = lessonEventType;
    }

    public LocalDate getEventDate() {
        return eventDate;
    }

    public void setEventDate(LocalDate eventDate) {
        this.eventDate = eventDate;
    }

}
