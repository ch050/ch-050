package com.school.journal.dto;

import java.time.LocalDate;

public class TeacherLessonEventDto {

    private String classId;

    private String subjectId;

    private LocalDate startDate;

    private LocalDate endDate;

    public TeacherLessonEventDto() {
    }

    public TeacherLessonEventDto(String classId, String subjectId, LocalDate startDate, LocalDate endDate) {
        this.classId = classId;
        this.subjectId = subjectId;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

}
