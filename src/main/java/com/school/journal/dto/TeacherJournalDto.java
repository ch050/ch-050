package com.school.journal.dto;

import java.util.List;

public class TeacherJournalDto {

    private List<TypeLessonEventDto> typeLessonEvents;

    private List<ChildJournal> childJournals;

    public TeacherJournalDto(List<TypeLessonEventDto> typeLessonEvents, List<ChildJournal> childJournals) {
        this.typeLessonEvents = typeLessonEvents;
        this.childJournals = childJournals;
    }

    public TeacherJournalDto() {
    }

    public List<ChildJournal> getChildJournals() {
        return childJournals;
    }

    public void setChildJournals(List<ChildJournal> childJournals) {
        this.childJournals = childJournals;
    }

    public List<TypeLessonEventDto> getTypeLessonEvents() {
        return typeLessonEvents;
    }

    public void setTypeLessonEvents(List<TypeLessonEventDto> typeLessonEvents) {
        this.typeLessonEvents = typeLessonEvents;
    }

}
