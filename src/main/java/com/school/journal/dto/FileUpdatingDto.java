package com.school.journal.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.web.multipart.MultipartFile;

public class FileUpdatingDto {

    private MultipartFile[] addedFiles;
    private String[] filenames;
    private String[] filesIdForRename;
    private String[] newNames;
    private String[] filesIdForRemove;
    private String subject;
    private String eventId;

    public FileUpdatingDto() {}

    public MultipartFile[] getAddedFiles() {
        return addedFiles;
    }
    public void setAddedFiles(MultipartFile[] addedFiles) {
        this.addedFiles = addedFiles;
    }

    public String[] getFilenames() {
        return filenames;
    }
    public void setFilenames(String[] filenames) {
        this.filenames = filenames;
    }

    public String[] getFilesIdForRename() {
        return filesIdForRename;
    }
    public void setFilesIdForRename(String[] filesIdForRename) {
        this.filesIdForRename = filesIdForRename;
    }

    public String[] getNewNames() {
        return newNames;
    }
    public void setNewNames(String[] newNames) {
        this.newNames = newNames;
    }

    public String[] getFilesIdForRemove() {
        return filesIdForRemove;
    }
    public void setFilesIdForRemove(String[] filesIdForRemove) {
        this.filesIdForRemove = filesIdForRemove;
    }

    public String getSubject() {
        return subject;
    }
    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getEventId() {
        return eventId;
    }
    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    @JsonIgnore
    public boolean hasNewFiles() {
        return addedFiles != null && filenames != null && addedFiles.length == filenames.length;
    }

    @JsonIgnore
    public boolean hasFilesForRename() {
        return filesIdForRename != null && newNames != null && filesIdForRename.length == newNames.length;
    }

    @JsonIgnore
    public boolean hasFilesForRemove() {
        return filesIdForRemove != null;
    }

}
