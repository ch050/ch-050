package com.school.journal.dto;

import java.util.List;

public class RelationshipDto {

    private NewChildDto newChild;
    private List<ParentDto> parents;

    public RelationshipDto() {
    }

    public RelationshipDto(NewChildDto newChild, List<ParentDto> parents) {
        this.newChild = newChild;
        this.parents = parents;
    }

    public NewChildDto getNewChild() {
        return newChild;
    }

    public void setNewChild(NewChildDto newChild) {
        this.newChild = newChild;
    }

    public List<ParentDto> getParents() {
        return parents;
    }

    public void setParents(List<ParentDto> parents) {
        this.parents = parents;
    }
}
