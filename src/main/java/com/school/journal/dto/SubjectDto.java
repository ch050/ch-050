package com.school.journal.dto;


import com.school.journal.model.events.Subject;

public class SubjectDto {

    private String id;

    private String name;

    public SubjectDto(Subject subject) {
        this.id = subject.getId();
        this.name = subject.getName();
    }

    public SubjectDto() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
