package com.school.journal.dto;

import com.school.journal.model.users.Teacher;

public class TeacherDto {

    private String id;

    private String lastName;

    private String firstName;

    private String patronymic;

    private String email;

    private String nickname;

    private String phoneNumber;

    private String description;

    public TeacherDto(Teacher teacher) {
        this(teacher.getId(), teacher.getLastName(),
                teacher.getFirstName(), teacher.getPatronymic(),
                teacher.getEmail(), teacher.getNickname(),
                teacher.getPhoneNumber(), teacher.getDescription());
    }

    public TeacherDto() {
    }

    public TeacherDto(String id, String lastName, String firstName, String patronymic) {
        this(id, lastName, firstName,
                patronymic, null,
                null, null, null);
    }

    public TeacherDto(String id, String lastName,
                      String firstName, String patronymic,
                      String email, String nickname,
                      String phoneNumber, String description) {
        this.id = id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.patronymic = patronymic;
        this.email = email;
        this.nickname = nickname;
        this.phoneNumber = phoneNumber;
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

}
