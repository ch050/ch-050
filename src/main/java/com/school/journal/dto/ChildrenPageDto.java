package com.school.journal.dto;

import com.school.journal.mapper.ChildRowMapper;
import com.school.journal.mapper.SchoolClassMapper;
import com.school.journal.model.users.Child;
import com.school.journal.model.users.SchoolClass;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ChildrenPageDto {

    private List<SchoolClassDto> schoolClasses = new ArrayList<>();
    private List<ChildRowDto> children = new ArrayList<>();

    public ChildrenPageDto() {
    }

    public ChildrenPageDto(List<SchoolClass> schoolClasses, List<Child> children) {
        this.schoolClasses = schoolClasses
                .stream()
                .map(SchoolClassMapper.MAPPER::toDto)
                .collect(Collectors.toList());

        this.children = children
                .stream()
                .map(ChildRowMapper.MAPPER::toDto)
                .collect(Collectors.toList());
    }

    public List<SchoolClassDto> getSchoolClasses() {
        return schoolClasses;
    }

    public void setSchoolClasses(List<SchoolClassDto> schoolClasses) {
        this.schoolClasses = schoolClasses;
    }

    public List<ChildRowDto> getChildren() {
        return children;
    }

    public void setChildren(List<ChildRowDto> children) {
        this.children = children;
    }
}
