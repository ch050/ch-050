package com.school.journal.dto;

public class LessonEventDto {

    private String classId;

    private String subjectId;

    private String eventDate;

    private String comment;

    private boolean completed;

    private String completionDate;

    private String lessonEventTypeId;

    public LessonEventDto() {
    }

    public LessonEventDto(String classId, String subjectId,
                          String eventDate, String comment,
                          boolean completed, String completionDate,
                          String lessonEventTypeId) {
        this.classId = classId;
        this.subjectId = subjectId;
        this.eventDate = eventDate;
        this.comment = comment;
        this.completed = completed;
        this.completionDate = completionDate;
        this.lessonEventTypeId = lessonEventTypeId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public String getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(String completionDate) {
        this.completionDate = completionDate;
    }

    public String getLessonEventTypeId() {
        return lessonEventTypeId;
    }

    public void setLessonEventTypeId(String lessonEventTypeId) {
        this.lessonEventTypeId = lessonEventTypeId;
    }
}
