package com.school.journal.dto;

import java.util.List;

public class ChildJournal {

    private String childName;

    private List<String> marks;

    public ChildJournal(String childName, List<String> marks) {
        this.childName = childName;
        this.marks = marks;
    }

    public ChildJournal() {
    }

    public String getChildName() {
        return childName;
    }

    public void setChildName(String childName) {
        this.childName = childName;
    }

    public List<String> getMarks() {
        return marks;
    }

    public void setMarks(List<String> marks) {
        this.marks = marks;
    }

}
