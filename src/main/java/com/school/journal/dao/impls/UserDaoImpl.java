package com.school.journal.dao.impls;

import com.school.journal.dao.interfaces.AbstractDao;
import com.school.journal.dao.interfaces.UserDao;
import com.school.journal.model.security.User;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;


@Repository(value = "UserDao")
public class UserDaoImpl extends AbstractDao<User> implements UserDao {

    public User findUserByUsername(String nickname) {
        TypedQuery<User> query = entityManager
                .createNamedQuery(User.FIND_BY_USERNAME, User.class).setParameter("nickname", nickname);
        return query.getSingleResult();
    }

}
