package com.school.journal.dao.impls;

import com.school.journal.dao.interfaces.AbstractDao;
import com.school.journal.dao.interfaces.LessonDao;
import com.school.journal.model.events.Lesson;
import com.school.journal.model.security.User;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import java.util.List;

@Repository(value = "LessonDao")
public class LessonDaoImpl extends AbstractDao<Lesson> implements LessonDao {

    @Override
    public List<Lesson> getLessonsByTeacherId(User user) {
        TypedQuery<Lesson> query = entityManager
                .createNamedQuery(Lesson.FIND_BY_TEACHER, Lesson.class).setParameter("teacherId", user.getId());
        return query.getResultList();
    }

    @Override
    public Lesson findByParameters(String userId, String classId, String subjectId) {
        TypedQuery<Lesson> query = entityManager
                .createNamedQuery(Lesson.FIND_BY_PARAMETERS, Lesson.class)
                .setParameter("teacherId", userId)
                .setParameter("classId", classId)
                .setParameter("subjectId", subjectId);
        return query.getSingleResult();
    }

    @Override
    public List<Lesson> getLessonsBySubject(String userId, String subjectId) {
        TypedQuery<Lesson> query = entityManager
                .createNamedQuery(Lesson.FIND_BY_SUBJECT, Lesson.class)
                .setParameter("teacherId", userId)
                .setParameter("subjectId", subjectId);
        return query.getResultList();
    }

}
