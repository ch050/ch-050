package com.school.journal.dao.impls;

import com.school.journal.dao.interfaces.AbstractDao;
import com.school.journal.dao.interfaces.LessonEventTypeDao;
import com.school.journal.model.events.LessonEventType;
import org.springframework.stereotype.Repository;

@Repository
public class LessonEventTypeDaoImpl extends AbstractDao<LessonEventType> implements LessonEventTypeDao {
}
