package com.school.journal.dao.impls;

import com.school.journal.dao.interfaces.AbstractDao;
import com.school.journal.dao.interfaces.SchoolClassDao;
import com.school.journal.model.users.SchoolClass;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SchoolClassDaoImpl extends AbstractDao<SchoolClass> implements SchoolClassDao {

    public List<SchoolClass> getActiveClasses() {
        return entityManager
                .createNamedQuery(SchoolClass.FIND_ACTIVE, SchoolClass.class)
                .getResultList();
    }
}
