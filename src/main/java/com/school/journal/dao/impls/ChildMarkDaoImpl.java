package com.school.journal.dao.impls;

import com.school.journal.dao.interfaces.AbstractDao;
import com.school.journal.dao.interfaces.ChildMarkDao;
import com.school.journal.model.events.ChildMark;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository(value = "ChildMarksDao")
public class ChildMarkDaoImpl extends AbstractDao<ChildMark> implements ChildMarkDao {
    @Override
    public List<ChildMark> getMarksByChildId(String childId) {
        return entityManager
                .createNamedQuery("ChildMark.getMarksByChildId", ChildMark.class)
                .setParameter("childId", childId)
                .getResultList();
    }
}
