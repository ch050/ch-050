package com.school.journal.dao.impls;

import com.school.journal.dao.interfaces.AbstractDao;
import com.school.journal.dao.interfaces.AuthorityDao;
import com.school.journal.model.security.Authority;
import org.springframework.stereotype.Repository;

@Repository
public class AuthorityDaoImpl extends AbstractDao<Authority> implements AuthorityDao{

    @Override
    public Authority findOne(Authority.AuthorityType type) {
        return entityManager.find(Authority.class , type );
    }

}
