package com.school.journal.dao.impls;

import com.school.journal.dao.interfaces.AbstractDao;
import com.school.journal.dao.interfaces.FileDAO;
import com.school.journal.model.events.FileResource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class FileDAOImpl extends AbstractDao<FileResource> implements FileDAO {

    @Override
    public List<FileResource> getFilesByEventId(String eventId) {
        return entityManager
                .createNamedQuery(FileResource.FIND_BY_EVENT, FileResource.class)
                .setParameter("event_id", eventId)
                .getResultList();
    }

}