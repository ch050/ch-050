package com.school.journal.dao.impls;

import com.school.journal.dao.interfaces.AbstractDao;
import com.school.journal.dao.interfaces.ParentDao;
import com.school.journal.model.users.Parent;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ParentDaoImpl extends AbstractDao<Parent> implements ParentDao {

    public List<Parent> getActiveParents() {
        return entityManager
                .createNamedQuery(Parent.FIND_ACTIVE, Parent.class)
                .getResultList();
    }
}
