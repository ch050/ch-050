package com.school.journal.dao.impls;

import com.school.journal.dao.interfaces.AbstractDao;
import com.school.journal.dao.interfaces.TeacherDao;
import com.school.journal.model.users.Teacher;
import org.springframework.stereotype.Repository;

@Repository
public class TeacherDaoImpl extends AbstractDao<Teacher> implements TeacherDao {
}
