package com.school.journal.dao.impls;

import com.school.journal.dao.interfaces.AbstractDao;
import com.school.journal.dao.interfaces.SubjectDao;
import com.school.journal.model.events.Subject;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class SubjectDaoImpl extends AbstractDao<Subject> implements SubjectDao{

    @Override
    public List<Subject> findByName(String name) {
        TypedQuery<Subject> query = entityManager
                .createNamedQuery(Subject.FIND_BY_NAME, Subject.class).setParameter("name", name);
        return query.getResultList();
    }

}
