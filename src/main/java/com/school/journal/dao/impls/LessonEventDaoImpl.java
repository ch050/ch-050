package com.school.journal.dao.impls;

import com.school.journal.dao.interfaces.AbstractDao;
import com.school.journal.dao.interfaces.LessonEventDao;
import com.school.journal.dto.TeacherLessonEventDto;
import com.school.journal.model.events.ChildMark;
import com.school.journal.model.events.LessonEvent;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Repository(value = "LessonEventDao")
public class LessonEventDaoImpl extends AbstractDao<LessonEvent> implements LessonEventDao {

    @Override
    public List<LessonEvent> findLessonEventsByChildIdAndDate(String childId, LocalDate date) {
        return entityManager
                .createNamedQuery(LessonEvent.FIND_BY_CHILD_ID_AND_DATE, LessonEvent.class)
                .setParameter("child_id", childId)
                .setParameter("date", date)
                .getResultList();
    }

    @Override
    public List<LessonEvent> getJournal(TeacherLessonEventDto lessonEventDto, String teacherId) {
        Query query = entityManager.createNamedQuery(LessonEvent.SELECT_JOURNAL);
        query.setParameter("subjectId",lessonEventDto.getSubjectId());
        query.setParameter("teacherId",teacherId);
        query.setParameter("classId",lessonEventDto.getClassId());
        query.setParameter("startDate",lessonEventDto.getStartDate());
        query.setParameter("endDate",lessonEventDto.getEndDate());
        List<LessonEvent> events = new ArrayList<>();
        for (Object object:query.getResultList()) {
            Hibernate.initialize(((LessonEvent) object).getLesson());
            Hibernate.initialize(((LessonEvent) object).getChildMarkList());
            Hibernate.initialize(((LessonEvent) object).getLessonEventType());
            for (ChildMark childMark :((LessonEvent) object).getChildMarkList()){
             Hibernate.initialize(childMark.getChild());
             Hibernate.initialize(childMark.getLessonEvent());
            }
            events.add((LessonEvent) object);
        }
        return events;
    }

}
