package com.school.journal.dao.interfaces;

import com.school.journal.model.users.Teacher;

public interface TeacherDao extends InterfaceDao<Teacher> {
}
