package com.school.journal.dao.interfaces;

import com.school.journal.model.users.SchoolClass;

import java.util.List;

public interface SchoolClassDao extends InterfaceDao<SchoolClass> {

    List<SchoolClass> getActiveClasses();

}
