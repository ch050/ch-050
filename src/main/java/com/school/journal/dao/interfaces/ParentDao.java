package com.school.journal.dao.interfaces;

import com.school.journal.model.users.Parent;

import java.util.List;

public interface ParentDao extends InterfaceDao<Parent> {

    List<Parent> getActiveParents();

}
