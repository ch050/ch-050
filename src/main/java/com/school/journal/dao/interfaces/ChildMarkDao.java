package com.school.journal.dao.interfaces;

import com.school.journal.model.events.ChildMark;

import java.util.List;

public interface ChildMarkDao extends InterfaceDao<ChildMark> {

    List<ChildMark> getMarksByChildId(String childId);

}
