package com.school.journal.dao.interfaces;

import com.school.journal.dto.TeacherLessonEventDto;
import com.school.journal.model.events.LessonEvent;

import java.time.LocalDate;
import java.util.List;

public interface LessonEventDao extends InterfaceDao<LessonEvent> {

     List<LessonEvent> getJournal(TeacherLessonEventDto lessonEventDto, String teacherId);

    List<LessonEvent> findLessonEventsByChildIdAndDate(String childId, LocalDate date);

}
