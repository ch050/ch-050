package com.school.journal.dao.interfaces;

import com.school.journal.model.events.Lesson;
import com.school.journal.model.security.User;

import java.util.List;

public interface LessonDao extends InterfaceDao<Lesson> {

    List<Lesson> getLessonsByTeacherId(User userByUsername);

    Lesson findByParameters(String userId, String classId, String subjectId);

    List<Lesson> getLessonsBySubject(String userId, String subjectId);

}
