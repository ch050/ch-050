package com.school.journal.dao.interfaces;

import com.school.journal.model.security.Authority;

public interface AuthorityDao extends InterfaceDao<Authority>{

    Authority findOne(Authority.AuthorityType type);

}
