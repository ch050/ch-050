package com.school.journal.dao.interfaces;

import com.school.journal.model.events.FileResource;

import java.util.List;

public interface FileDAO extends InterfaceDao<FileResource> {
    List<FileResource> getFilesByEventId(String eventId);
}
