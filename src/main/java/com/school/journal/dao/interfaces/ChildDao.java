package com.school.journal.dao.interfaces;

import com.school.journal.model.users.Child;
import com.school.journal.model.users.Parent;

import java.util.List;

public interface ChildDao extends InterfaceDao<Child> {

    List<Child> getActiveChildren();
    List<Child> getChildrenByClass(String classId);

}
