package com.school.journal.dao.interfaces;


import com.school.journal.model.events.Subject;

import java.util.List;

public interface SubjectDao extends InterfaceDao<Subject> {

    List<Subject> findByName(String name);

}
