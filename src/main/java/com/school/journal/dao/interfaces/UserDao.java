package com.school.journal.dao.interfaces;


import com.school.journal.model.security.User;

public interface UserDao extends InterfaceDao<User> {

    User findUserByUsername(String nickname);

}
