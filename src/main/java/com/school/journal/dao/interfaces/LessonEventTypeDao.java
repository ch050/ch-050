package com.school.journal.dao.interfaces;

import com.school.journal.model.events.LessonEventType;

public interface LessonEventTypeDao extends InterfaceDao<LessonEventType> {
}
