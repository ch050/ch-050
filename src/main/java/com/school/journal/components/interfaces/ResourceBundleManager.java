package com.school.journal.components.interfaces;

import java.util.*;

public interface ResourceBundleManager {
    Map<String, String> getWords(String page, Locale locale);

    static Map<String, String> toMap(ResourceBundle resourceBundle) {
        Map<String, String> map = new HashMap<>();

        Enumeration<String> keys = resourceBundle.getKeys();
        while (keys.hasMoreElements()) {
            String key = keys.nextElement();
            map.put(key, resourceBundle.getString(key));
        }

        return map;
    }

    @SafeVarargs
    static Map<String, String> mergeMaps(Map<String, String>... maps) {
        final HashMap<String, String> resultMap = new HashMap<>();

        for (Map<String, String> map : maps) {
            resultMap.putAll(map);
        }

        return resultMap;
    }
}