package com.school.journal.components.interfaces;

import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Map;

public interface PagesManager {

    Map<String, String> getAllowedPages(Collection<? extends GrantedAuthority> grantedAuthorities);
    String getMainPage(Collection<? extends GrantedAuthority> grantedAuthorities);

}