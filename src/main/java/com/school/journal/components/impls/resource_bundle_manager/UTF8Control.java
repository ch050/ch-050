package com.school.journal.components.impls.resource_bundle_manager;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class UTF8Control extends ResourceBundle.Control {
    public ResourceBundle newBundle(String baseName, Locale locale, String format, ClassLoader loader, boolean reload)
            throws IllegalAccessException, InstantiationException, IOException {
        final String resourceName = toResourceName(toBundleName(baseName, locale), "properties");
        InputStream stream = null;

        try  {
            stream = reload ? getReload(loader, resourceName) : loader.getResourceAsStream(resourceName);

            if (stream == null) {
                return null;
            }

            return new PropertyResourceBundle(new InputStreamReader(stream, "UTF-8"));
        } finally {
            IOUtils.closeQuietly(stream);
        }

    }

    private InputStream getReload(ClassLoader loader, String resourceName) throws IOException {
        final URL url = loader.getResource(resourceName);
        final URLConnection con = url == null ? null : url.openConnection();

        if (con == null) {
            return null;
        }

        con.setUseCaches(false);
        return con.getInputStream();
    }
}