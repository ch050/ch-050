package com.school.journal.components.impls;

import com.school.journal.components.interfaces.PagesManager;
import com.school.journal.model.security.Authority;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static com.school.journal.model.security.Authority.*;

@Component
public class PagesManagerImpl implements PagesManager {

    @Value("#{${header.pages.admin}}")
    private Map<String, String> adminPages;

    @Value("#{${header.pages.teacher}}")
    private Map<String, String> teacherPages;

    @Value("#{${header.pages.parent}}")
    private Map<String, String> parentPages;

    @Value("#{${header.pages.child}}")
    private Map<String, String> childPages;


    @Value("${header.main_page.admin}")
    private String adminMainPage;

    @Value("${header.main_page.teacher}")
    private String teacherMainPage;

    @Value("${header.main_page.parent}")
    private String parentMainPage;

    @Value("${header.main_page.child}")
    private String childMainPage;

    public Map<String, String> getAllowedPages(Collection<? extends GrantedAuthority> grantedAuthorities) {
        Map<String, String> allowedPages = new HashMap<>();

        grantedAuthorities.forEach(grantedAuthority -> {
            AuthorityType authority = AuthorityType.valueOf(grantedAuthority.getAuthority());

            switch (authority) {
                case ADMIN: allowedPages.putAll(adminPages); break;
                case TEACHER: allowedPages.putAll(teacherPages); break;
                case PARENT: allowedPages.putAll(parentPages); break;
                case CHILD: allowedPages.putAll(childPages); break;
            }
        });

        return allowedPages;
    }

    public String getMainPage(Collection<? extends GrantedAuthority> grantedAuthorities) {

        if (grantedAuthorities.isEmpty()) {
            return "/";
        }

        AuthorityType authority = AuthorityType.valueOf(new ArrayList<>(grantedAuthorities).get(0).getAuthority());

        switch (authority) {
            case ADMIN: return adminMainPage;
            case TEACHER: return teacherMainPage;
            case PARENT: return parentMainPage;
            case CHILD: return childMainPage;

            default: throw new IllegalArgumentException("Unknown authority");
        }

    }

}
