package com.school.journal.components.impls.resource_bundle_manager;

import com.school.journal.components.interfaces.ResourceBundleManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.nio.file.Paths;
import java.util.*;

@Component
public class ResourceBundleManagerImpl implements ResourceBundleManager {

    @Value("${msg.directory}")
    private String directory;

    @Value("${msg.name}")
    private String name;

    @Value("${msg.languages}")
    private List<String> languages;

    @Value("${msg.pages}")
    private List<String> pages;

    @Value("${msg.header_prefix}")
    private String headerPrefix;

    private final TranslationWords words = new TranslationWords();

    @PostConstruct
    public void init() {
        final String baseName = Paths.get(directory, name).toString();

        languages.forEach(language -> {
            Locale locale = new Locale(language);
            ResourceBundle resourceBundle = ResourceBundle.getBundle(baseName, locale, new UTF8Control());

            Map<String, String> words = ResourceBundleManager.toMap(resourceBundle);
            Map<String, String> headerMessages = filterByPrefix(headerPrefix, words);

            pages.forEach(page -> {
                Map<String, String> translations = ResourceBundleManager.mergeMaps(filterByPrefix(page, words), headerMessages);
                this.words.put(page, locale, translations);
            });
        });

    }

    @Override
    public Map<String, String> getWords(String page, Locale locale) {
        return words.get(page, locale);
    }

    private Map<String, String> filterByPrefix(String prefix, Map<String, String> words) {
        Map<String, String> filteredMap = new HashMap<>();

        final String finalPrefix = prefix + '.';
        words.forEach((key, value) -> {
            if (key.contains(finalPrefix)) {
                filteredMap.put(key.replaceFirst(finalPrefix, ""), value);
            }
        });

        return filteredMap;
    }

}