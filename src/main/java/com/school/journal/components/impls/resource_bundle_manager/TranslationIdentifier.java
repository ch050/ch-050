package com.school.journal.components.impls.resource_bundle_manager;

import java.util.Locale;

public class TranslationIdentifier {

    private String page;
    private Locale locale;

    public TranslationIdentifier(String page, Locale locale) {
        this.page = page;
        this.locale = locale;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TranslationIdentifier that = (TranslationIdentifier) o;

        if (!page.equals(that.page)) return false;
        return locale.equals(that.locale);
    }

    @Override
    public int hashCode() {
        int result = page.hashCode();
        result = 31 * result + locale.hashCode();
        return result;
    }
}