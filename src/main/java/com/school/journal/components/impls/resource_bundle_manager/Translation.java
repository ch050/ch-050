package com.school.journal.components.impls.resource_bundle_manager;

import java.util.HashMap;
import java.util.Map;

public class Translation {
    private Map<String, String> translations = new HashMap<>();

    public Translation(Map<String, String> translations) {
        this.translations = translations;
    }

    public Map<String, String> getTranslations() {
        return translations;
    }

}