package com.school.journal.components.impls.resource_bundle_manager;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class TranslationWords {
    private Map<TranslationIdentifier, Translation> translations = new HashMap<>();

    public void put(String page, Locale locale, Map<String, String> translations) {
        this.translations.put(new TranslationIdentifier(page, locale), new Translation(translations));
    }

    public Map<String, String> get(String page, Locale locale) {
        Translation translation = this.translations.get(new TranslationIdentifier(page, locale));
        return translation == null ? new HashMap<>() : translation.getTranslations();
    }

}