package com.school.journal.validation;

import com.school.journal.model.security.User;
import com.school.journal.model.users.Child;
import com.school.journal.model.users.Parent;
import com.school.journal.model.users.Teacher;

import java.util.regex.Pattern;

public class UserValidation {

    private static final Pattern NAME_PATTERN = Pattern.compile("^[а-яА-ЯёЁa-zA-Z-]{3,64}$");
    private static final Pattern PATRONYMIC_PATTERN = Pattern.compile("^[а-яА-ЯёЁa-zA-Z']{0,64}$");
    private static final Pattern NICKNAME_PATTERN = Pattern.compile("^[a-zA-Z][a-zA-Z0-9-_.]{3,36}$");
    private static final Pattern EMAIL_PATTERN = Pattern.compile("^[-a-z0-9!#$%&'*+/=?^_`{|}~]+(\\.[-a-z0-9!#$%&'*+/=?^_`{|}~]+)*@([a-z0-9]([-a-z0-9]{0,61}[a-z0-9])?\\.)*(aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|[a-z][a-z])$");
    private static final Pattern PHONE_PATTERN = Pattern.compile("^((\\+\\d{2})?\\d{10})?$");
    private static final Pattern JOB_PATTERN = Pattern.compile("^.{0,255}$");

    /**
     * This method checks if value that user entered
     * is more than 3 characters but less than 64
     * and if it contains only letters (hyphen allowed too)
     *
     * @param name contains user first name or last name
     * @return true if param matches the up-mentioned rules
     */

    public static boolean isNameCorrect(String name) {
        return NAME_PATTERN.matcher(name).matches();
    }

    /**
     * This method checks if value that user entered
     * is less than 64 characters (it can also be empty)
     * and if it contains only letters and apostrophes
     *
     * @param patronymic contains user patronymic
     * @return true if param matches the up-mentioned rules
     */

    public static boolean isPatronymicCorrect(String patronymic) {
        return PATRONYMIC_PATTERN.matcher(patronymic).matches();
    }

    /**
     * This method checks if value that user entered
     * is between 3 and 36 characters
     * it can contain letters (upper- and lowercase),
     * numbers, underscores and dots
     *
     * @param nickname contains user nickname
     * @return true if param matches the up-mentioned rules
     */

    public static boolean isNicknameCorrect(String nickname) {
        return NICKNAME_PATTERN.matcher(nickname).matches();
    }

    /**
     * This method checks if value that user entered
     * matches common standard for email
     *
     * @param email contains user email
     * @return true if param matches the up-mentioned rules
     */

    public static boolean isEmailCorrect(String email) {
        return EMAIL_PATTERN.matcher(email).matches();
    }

    /**
     * This method checks if value that user entered
     * contains only numbers (plus allowed too)
     * (it can also be empty)
     *
     * @param phoneNumber contains user phone number
     * @return true if param matches the up-mentioned rules
     */

    public static boolean isPhoneNumberCorrect(String phoneNumber) {
        return PHONE_PATTERN.matcher(phoneNumber).matches();
    }

    /**
     * This method checks if value that user entered
     * contains less than 255 characters (it can also be empty)
     *
     * @param job contains user job description
     * @return true if param matches the up-mentioned rules
     */

    public static boolean isJobCorrect(String job) {
        return JOB_PATTERN.matcher(job).matches();
    }

    /**
     * This method checks if user object fields
     * matches the validation rules
     *
     * @param user contains object that needs to be checked
     * @return true if user fields are valid
     */

    private static boolean checkCommonInfo(User user) {
        return (isNameCorrect(user.getLastName()) &&
                isNameCorrect(user.getFirstName()) &&
                isPatronymicCorrect(user.getPatronymic()) &&
                isNicknameCorrect(user.getNickname()) &&
                isEmailCorrect(user.getEmail()) &&
                isPhoneNumberCorrect(user.getPhoneNumber()));
    }

    public static boolean checkChild(Child child) {
        return checkCommonInfo(child);
    }

    public static boolean checkParent(Parent parent) {
        return checkCommonInfo(parent) && isJobCorrect(parent.getJob());
    }

    public static boolean checkTeacher(Teacher teacher) {
        return checkCommonInfo(teacher);
    }

}
