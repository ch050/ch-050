package com.school.journal.validation;

import java.util.regex.Pattern;

public class SchoolClassValidation {

    private static final Pattern CLASS_NAME_PATTERN = Pattern.compile("^\\d{1,2}[-а-яА-Яa-zA-Z]{0,2}$");

    public static boolean isClassNameCorrect(String name) {
        return CLASS_NAME_PATTERN.matcher(name).matches();
    }

}
