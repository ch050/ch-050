package com.school.journal.validation;

import com.school.journal.dao.impls.SubjectDaoImpl;
import com.school.journal.dao.interfaces.SubjectDao;
import com.school.journal.model.events.Subject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SubjectValidator {

    private static final Pattern NAME_PATTERN = Pattern.compile("^[а-яА-ЯёЁa-zA-Z-]{3,64}$");

    private static boolean isNameCorrect(String name) {
        Matcher matcher = NAME_PATTERN.matcher(name);
        return matcher.matches();
    }

    public static boolean validate(Subject subject) {
        return (isNameCorrect(subject.getName()));
    }

}
