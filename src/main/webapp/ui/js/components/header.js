var header = {
    nickname: 'name1',
    email: 'mail@gmail.com',
    language: 'en',
    currentPage: 'test',
    mainPage: '/',
    pages: {},
    messages: {},
    loadedMessages: [],
    selectedLanguage: 'en',
    selectedLanguageElement: '',
    token: ''
};

Vue.component('main-header', {
    template: '<div class="navbar navbar-default navbar-fixed-top" role="navigation" id="navbar" >' +
    '<div class="container-fluid">' +
    '<div class="navbar-header">' +
    '<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="navbar-collapse">' +
    '<span class="icon-bar"></span>' +
    '<span class="icon-bar"></span>' +
    '<span class="icon-bar"></span>' +
    '</button>' +
    '<a v-bind:href="mainPage" class="navbar-brand"><img class= "" src="/ui/images/SJ.png" style="height: 20px;width: 40px;"></a>' +
    '</div>' +
    '<div class="collapse navbar-collapse">' +
    '<ul class="nav navbar-nav">' +
    '<li v-for="(value, key) in pages" ><a v-bind:href="value">{{ messages[key] }}</a></li>' +
    '</ul>' +

    '<ul class="nav navbar-nav navbar-right">' +
    '<li style="margin-top:11px" >' +
    '<button id="langEn" class="btn-link" v-on:click="changeLanguage(\'en\')" ><img src="/ui/images/gbFlag.png" style="height: 20px;width: auto" alt="header.messages.english"></button>' +
    '<button id="langUk" class="btn-link" v-on:click="changeLanguage(\'ua\')" ><img src="/ui/images/ukFlag.png" style="height: 20px;width: auto" alt="header.messages.ukrainian "></button>' +
    '</li>' +
    '<li class="dropdown">' +
    '<a href="#" class="dropdown-toggle" data-toggle="dropdown">' +
    '<span class="glyphicon glyphicon-user"></span> ' +
    '<strong>{{ nickname }}</strong>' +
    '<span class="glyphicon glyphicon-chevron-down"></span>' +
    '</a>' +
    '<ul class="dropdown-menu">' +
    '<li>' +
    '<div class="navbar-login">' +
    '<div class="row">' +
    '<div class="col-lg-4 col-md-4">' +
    '<p class="text-center">' +
    '<span class="glyphicon glyphicon-user icon-size"></span>' +
    '</p>' +
    '</div>' +
    '<div class="col-lg-8">' +
    '<p class="text-left"><strong>{{ nickname }}</strong></p>' +
    '<p class="text-left small">{{ email }}</p>' +
    '<p>' +
    '<a href="/logout" class="btn btn-danger btn-block">{{ messages.logout }}</a>' +
    '</p>' +
    '</div>' +
    '</div>' +
    '</div>' +
    '</li>' +
    '</ul>' +
    '</li>' +
    '</ul>' +
    '</div>' +
    '</div>' +
    '</div>',
    data: function () {
        return header;
    },
    mounted: function () {
        header.token = this.readCookie('XSRF-TOKEN');
        Vue.http.headers.common['X-XSRF-TOKEN'] = header.token;


        let savedLanguage = sessionStorage.getItem('language');
        if (savedLanguage) this.language = savedLanguage;

        this.$http.post('/header/getData', {page: this.currentPage, language: this.language}, {
            emulateJSON: true
        }).then(
            function (response) {
                this.nickname = response.body.nickname;
                this.email = response.body.email;
                this.language = response.body.language;
                this.mainPage = response.body.mainPage;
                this.pages = response.body.pages;
                this.messages = response.body.messages;
                this.loadedMessages.push({
                    language: this.language,
                    messages: this.messages
                });
                sessionStorage.setItem('language', this.language);
            },
            function (error) {}
        )
    },
    methods: {
        changeLanguage: function (language) {
            if (this.language === language) return;
            let loadedMessages = this.getLoadedMessages(language);
            if (loadedMessages) {
                this.messages = loadedMessages;
                this.language = language;
                return;
            }

            this.$http.post('/header/loadLanguage/' + language, { page: this.currentPage }, {
                emulateJSON: true
            }).then(
                function (response) {
                    this.messages = response.body;
                    this.language = language;

                    this.loadedMessages.push({
                        messages: this.messages,
                        language: this.language
                    });

                    sessionStorage.setItem('language', this.language);
                },
                function (error) {}
            );
        },
        getLoadedMessages: function (language) {
            for (let i = 0; i < this.loadedMessages.length; i++) {
                if (this.loadedMessages[i].language === language) return this.loadedMessages[i].messages;
            }

            return null;
        },
        readCookie: function (name) {
            let nameEQ = name + "=";
            let ca = document.cookie.split(';');
            for (let i = 0; i < ca.length; i++) {
                let c = ca[i];
                while (c.charAt(0) === ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        }
    }
});