var childrenApp = new Vue({
    el: '#app',
    data: {
        schoolClasses: [],
        selectedClass: 'All children',
        childrenTableTitle: 'All children',
        columns: ['School class', 'Last name', 'First name', 'Patronymic'],
        allChildren: [],
        children: [],
        header: header
    },
    methods: {
        goToAddChild: function () {
            parent.location = "/children/addChild";
        },
        onSelectedClass: function () {
            if (this.selectedClass === 'All children') {
                this.childrenTableTitle = 'All children';
                this.children = this.allChildren.slice();
            } else {
                this.childrenTableTitle = this.selectedClass.name;
                var id = this.selectedClass.id;
                var childrenByClass = [];
                this.allChildren.forEach(function (child) {
                    if (child.schoolClass.id === id) childrenByClass.push(child);
                });
                this.children = childrenByClass;
            }
        },
        moreInfo: function (child) {
            var childId = child.id;
        },
        editChild: function (child) {
            var childId = child.id;
            window.sessionStorage.setItem('childId', childId);
            parent.location = '/children/editChild';
        },
        deleteChild: function (child) {
            function postConfirmAction() {
                var childId = child.id;
                childrenApp.$http.post('/children/deactivate', { childId: childId }, {
                    emulateJSON: true
                }).then(function (response) {
                    var index = this.children.indexOf(child);
                    this.children.splice(index, 1);
                }, function (response) {
                    alert("Error during deactivating!")
                });
            }
            var childFullName = child.lastName + ' ' + child.firstName + ' ' + child.patronymic;
            this.confirmDelete(postConfirmAction, childFullName);

        },
        confirmDelete: function (onConfirm, childName) {
            $.confirm({
                columnClass: 'medium',
                title: '',
                content: '<span class="confirm-default-text">Do you want to deactivate <span class="confirm-name-text">' + childName + '</span>?</span>',
                buttons: {
                    confirm: onConfirm,
                    cancel: function () {}
                }
            });
        }
    },
    created: function () {
        header.currentPage = 'children';
    },
    mounted: function () {
        this.$http.get('/children/getAll').then(function (response) {
                this.schoolClasses = response.body.schoolClasses;
                this.allChildren = response.body.children;
                this.children = this.allChildren.slice();
            },
            function (response) {
                alert("Can't load data from the database");
            }
        );
    }

});


