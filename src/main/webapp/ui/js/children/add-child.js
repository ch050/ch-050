var addChildApp = new Vue({
    el: '#app',
    data: {
        schoolClasses: {},
        selectedClassId: '',
        child: {
            lastName: '',
            firstName: '',
            patronymic: '',
            nickname: '',
            email: '',
            phoneNumber: ''
        },
        parents: [],
        header: header
    },
    methods: {
        addParent: function () {
            var parent = {
                lastName: '',
                firstName: '',
                patronymic: '',
                nickname: '',
                email: '',
                phone: '',
                job: ''
            };
            this.parents.push(parent);
        },
        saveChild: function () {

            this.child.schoolClassId = this.selectedClassId;

            var data = {
                newChild: this.child,
                parents: this.parents
            };

            this.$http.post('/children/add', data).then(function (response) {
                if (response.body === '') {
                    alert("Please add parent for child!");
                } else {
                    alert("Success! Generated password for child: " + response.body);
                }
                parent.location = '/children/all';
            }, function (response) {
                alert("Error during creating");
            });

        },
        cancelSaving: function () {
            parent.location = '/children/all';
        },
        removeParent: function (parent) {
            function postConfirmAction() {
                var index = addChildApp.parents.indexOf(parent);
                addChildApp.parents.splice(index, 1);
            }
            this.confirmDelete(postConfirmAction);

        },
        confirmDelete: function (onConfirm) {
            $.confirm({
                columnClass: 'medium',
                title: '',
                content: '<span class="confirm-default-text">Do you want to delete this parent? You can lose your changes.</span>',
                buttons: {
                    confirm: onConfirm,
                    cancel: function () {}
                }
            });
        }
    },
    created: function () {
        header.currentPage = 'add_child';
    },
    mounted: function () {
        this.$http.get('/children/getAll').then(function (response) {
                this.schoolClasses = response.body.schoolClasses;
            },
            function (response) {
                alert("Can't load data from the database");
            }
        )
    }

});

$.validator.setDefaults( {
    submitHandler: function () {
        addChildApp.saveChild();
    }
} );

$("#add-child-form").validate({
    rules: {
        childLastName: {
            required: true,
            minlength: 3,
            username: true
        },
        parentLastName: {
            required: true,
            minlength: 3,
            username: true
        },
        childFirstName: {
            required: true,
            minlength: 3,
            username: true
        },
        parentFirstName: {
            required: true,
            minlength: 3,
            username: true
        },
        childPatronymic: {
            required: false,
            patronymic: true
        },
        parentPatronymic: {
            required: false,
            patronymic: true
        },
        childNickname: {
            required: true,
            nickname: true,
            minlength: 2
        },
        parentNickname: {
            required: true,
            nickname: true,
            minlength: 2
        },
        childEmail: {
            required: true,
            email: true
        },
        parentEmail: {
            required: true,
            email: true
        },
        childPhone: {
            required: true,
            phone: true
        },
        parentPhone: {
            required: true,
            phone: true
        },
        parentJob: {
            required: false,
            maxlenght: 255
        }
    },
    messages: {
        childLastName: {
            required: "Please enter child last name",
            minlength: "Last name be between 3 and 64 characters"
        },
        parentLastName: {
            required: "Please enter parent last name",
            minlength: "Last name be between 3 and 64 characters"
        },
        childFirstName: {
            required: "Please enter child first name",
            minlength: "First name be between 3 and 64 characters"
        },
        parentFirstName: {
            required: "Please enter parent first name",
            minlength: "First name be between 3 and 64 characters"
        },
        childNickname: {
            required: "Please enter a child nickname",
            minlength: "Nickname must consist of at least 3 characters"
        },
        parentNickname: {
            required: "Please enter a parent nickname",
            minlength: "Nickname must consist of at least 3 characters"
        },
        childEmail: {
            required: "Please enter a child email",
            email: "Please enter a valid email address"
        },
        parentEmail: {
            required: "Please enter a parent email",
            email: "Please enter a valid email address"
        },
        childPhone: {
            required: "Please enter a phone number",
            phone: "Please enter a phone number in XXXXXXXXXX or +XXXXXXXXXXXX format"
        },
        parentPhone: {
            required: "Please enter a phone number",
            phone: "Please enter a valid phone number"
        },
        parentJob: {
            maxlenght: "Job description should be shorter than 255 characters"
        }
    },
    errorElement: "em",
    errorPlacement: function (error, element) {
        error.addClass("help-block");
        element.parents(".col-sm-5").addClass("has-feedback");
        if (element.prop("type") === "checkbox") {
            error.insertAfter(element.parent("label"));
        } else {
            error.insertAfter(element);
        }
        if (!element.next("span")[0]) {
            $("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").insertAfter(element);
        }
    },
    success: function (label, element) {
        if (!$(element).next("span")[0]) {
            $("<span class='glyphicon glyphicon-ok form-control-feedback'></span>").insertAfter($(element));
        }
    },
    highlight: function (element, errorClass, validClass) {
        $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
        $(element).next("span").addClass("glyphicon-remove").removeClass("glyphicon-ok");
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
        $(element).next("span").addClass("glyphicon-ok").removeClass("glyphicon-remove");
    }

});

jQuery.validator.addMethod("username", function(value, element) {
    return this.optional(element) || /^[а-яА-ЯёЁa-zA-Z-]{3,64}$/.test(value);
}, 'Name should consist only of letters and hyphens');

jQuery.validator.addMethod("patronymic", function(value, element) {
    return this.optional(element) || /^[а-яА-ЯёЁa-zA-Z']{0,64}$/.test(value);
}, 'Patronymic consist only of letters and apostrophes');

jQuery.validator.addMethod("nickname", function(value, element) {
    return this.optional(element) || /^[a-zA-Z][a-zA-Z0-9-_.]{2,36}$/.test(value);
}, 'Nickname should consist only latin letters, numbers, underscores and dots');

jQuery.validator.addMethod("phone", function(value, element) {
    return this.optional(element) || /^((\+\d{2})?\d{10})?$/.test(value);
});

jQuery.validator.addMethod("email", function(value, element) {
    return this.optional(element) || /^[-a-z0-9!#$%&'*+/=?^_`{|}~]+(\.[-a-z0-9!#$%&'*+/=?^_`{|}~]+)*@([a-z0-9]([-a-z0-9]{0,61}[a-z0-9])?\.)*(aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|[a-z][a-z])$/.test(value);
});