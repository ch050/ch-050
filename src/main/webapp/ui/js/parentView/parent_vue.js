Vue.component('paginate', VuejsPaginate);
var parent_view = new Vue({
  el: '#parent-script',
  data:	{

    parentNick: '',
    parentMail: '',

    currentChild: {},
    childInfo: [],
    block: [],

    totalPages: '',
    currentPage: 1,
    perPage: 7,
    first: 0,
    last: 3,

    startDate: '',
    finishDate: '',

    currentChildIndex: '',

    header: header
  },

  created: function () {
    header.currentPage = 'parent_marks';
  },

  mounted: function() {
      let dateObj = {
          format: 'yyyy-mm-dd',
          todayHighlight: true,
          autoclose: true,
          orientation: 'bottom'
      };
      $("#startDate").datepicker(dateObj);
      $("#finishDate").datepicker(dateObj);

      this.loadData();
    },
    methods: {
      setChild: function(child){
        this.currentChild = child;
        for (let i = 0; i < this.childInfo.length; i++){
            if (this.childInfo[i].id == this.currentChild.id)
                this.currentChildIndex = i;
        }
          this.totalPages = this.childInfo[this.currentChildIndex].dates.length/this.perPage;
      },

      clickCallback: function(pageNum) {
        this.block = [];

        if (pageNum == 1) {
          this.first = 0;
          this.last = this.perPage-1;
        }

        else {
          this.last = pageNum*this.perPage-1;
          this.first = this.last-this.perPage+1;
        }

        for (let i = this.first; i < this.last+1; i++) {
          if (this.childInfo[this.currentChildIndex].dates[i]!=null) {
              this.block.push({
                  date: this.childInfo[this.currentChildIndex].dates[i],
                  marks: this.childInfo[this.currentChildIndex].marks[i]
              });
          }
        }
      },
      extract: function (array, param) {
          let result = [];
          for (let obj of array) {
              if (param === "dates" && this.validDate(obj.date)) {
                  result.push(obj.date);
              }
              if (param === "marks" && this.validDate(obj.date)) {
                  result.push(obj.marks);
              }
          }
          return result;
      },
      paintMark: function(mark){
            if (mark <= 6){
                mark = "low-mark"
            }
            if (mark <= 9 && mark >= 7){
                mark = "medium-mark"
            }
            if (mark >= 10){
                mark = "high-mark"
            }
          return mark;
        },

        loadData: function () {
            this.childInfo = [];

            this.$http.get('/parent/children/info').then(function (response) {
                this.parentNick = response.body.parentName + ' ';
                this.parentMail = response.body.eMail + ' ';

                for (let i = 0; i < response.body.childrenData.length; i++) {
                    let activeTab = i==0 ? 'tab-pane fade in active' : 'tab-pane fade';

                    this.childInfo.push({
                        id: response.body.childrenData[i].childId,
                        name: response.body.childrenData[i].childName,
                        tabId: '#tab'+(i+1),
                        tabName: 'tab'+(i+1),
                        subjects: response.body.childrenData[i].subjects,
                        dates: this.extract(response.body.childrenData[i].marks, "dates"),
                        marks: this.extract(response.body.childrenData[i].marks, "marks"),
                        activeTab: activeTab
                    });
                }
                this.totalPages = 0;
            });
        },

        validDate: function (date) {
          if (this.startDate == '' && this.finishDate == '') {
              return true;
          }

          let start = moment(this.startDate, "YYYY-MM-DD").format('X');
          let current = moment(date, "YYYY-MM-DD").format('X');
          let finish = moment(this.finishDate, "YYYY-MM-DD").format('X');

        return (start <= current) && (current <= finish) && (start < finish);
        },

        clear:function () {
            this.startDate = '';
            this.finishDate = '';
        },

        currentWeek: function () {
            let curr = new Date;
            this.startDate = moment(new Date(curr.setDate(curr.getDate() - curr.getDay()+1))).format("YYYY-MM-DD");
            this.finishDate = moment(new Date(curr.setDate(curr.getDate() - curr.getDay()+7))).format("YYYY-MM-DD");
        }
    }
});
