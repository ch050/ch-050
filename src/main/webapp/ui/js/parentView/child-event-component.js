const component_data = {
    isLoaded: false,
    columns: ['subject', 'comment', 'files'],
    events: [{a: 'aa'}],
    messages: {
    }
};
Vue.component('child-events-modal', {
    template: '<div>' +
    '<button class="btn-link" v-on:click="init(childId, date)" data-toggle="modal" data-target="#upload-modal">' +
    '{{ date }}' +
    '</button>' +
    '<div class="modal" id="upload-modal" tabindex="-1" role="dialog">' +
    '<div class="modal-dialog modal-lg">' +
    '<div class="modal-content">' +
    '<div class="modal-header">' +
    '<button id="close-modal-btn" type="button" class="close" data-dismiss="modal">&times;</button>' +
    '</div>' +
    '<div class="modal-body">' +
    '<table class="table table-bordered text-center">' +
    '<thead>' +
    '<tr class="bg-primary" >' +
    '<th class="text-center" v-for="column in columns" >{{ column }}</th>' +
    '</tr>' +
    '</thead>' +
    '<tbody>' +
    '<tr v-for="event in events" >' +
    '<td class="col-md-1 vertical-middle" >{{ event.subject }}</td>' +
    '<td class="col-md-6" >{{ event.comment }}</td>' +
    '<td class="col-md-1">' +
    '<div v-for="file in event.files" >' +
    '<a v-bind:href="downloadLink(file.id)">{{ file.name }}</a> ' +
    '</div>' +
    '</td>' +
    '</tr>' +
    '</tbody>' +
    '</table>' +
    '</div>' +
    '</div>' +
    '</div>' +
    '</div>' +
    '</div>',

    props: ['date', 'child-id'],
    data: function () {
        return component_data
    },
    methods: {
        downloadLink: function (fileId) {
            return '/files/download/' + fileId;
        },
        init(childId, date) {
            if (component_data.isLoaded) return;

            let data = {
                childId: childId,
                date: date
            };

            this.$http.post('/parent/getChildEvents', data, { emulateJSON: true }).then(
                function (response) {
                    component_data.events = response.body;
                    component_data.isLoaded = true;
                },
                function (response) {}
            );
        }
    }
});