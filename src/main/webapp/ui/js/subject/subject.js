Vue.component('modal', {
    template: '#modal-template'
});
var subjectApp = new Vue({
    el: '#app',
    data: {
        selectedSubjectName: '',
        subjects: [],
        showModal: false,
        id: null,
        nameSubject: '',
        isAdd: false,
        header:header,
    },
    methods: {
        deleteSubject: function (subject) {
            function postConfirmAction() {
                subjectApp.$http.delete("/subjects/" + subject.id);
                var index = subjectApp.subjects.indexOf(subject);
                subjectApp.subjects.splice(index, 1);
            }
            this.confirmDelete(postConfirmAction);
        },
        confirmDelete: function (onConfirm) {
            $.confirm({
                columnClass: 'medium',
                title: '',
                content: '<span class="confirm-default-text">Do you want to delete subject? You can lose your changes.</span>',
                buttons: {
                    confirm: onConfirm,
                    cancel: function () {
                    }
                }
            });
        },
        editSubject: function (subject) {
            this.isAdd = true;
            this.nameSubject = subject.name;
            this.id = subject.id;
            var index = this.subjects.indexOf(subject);
            this.subjects.splice(index, 1);
        },
        addSubject: function () {
            this.isAdd = true;
        },
        saveSubject: function () {
            var subject = {
                id: this.id,
                name: this.nameSubject
            };

            if (this.id === null) {
                this.$http.post('/subjects', subject);
                this.subjects.push(subject);
            }
            else {
                this.$http.put('/subjects', subject);
                this.subjects.push(subject);
            }
            this.id = null;
            this.nameSubject = null;
            this.isAdd = false;
        }
    },
    created: function () {
        header.currentPage = "subjects";
    },
    mounted: function () {
        this.$http.get('/subject').then(function (response) {
                this.subjects = response.body;
            },
            function (response) {
                alert("Can't load data from the database");
            }
        );
    }

});

$.validator.setDefaults( {
    submitHandler: function () {
        subjectApp.saveSubject();
    }
} );

$("#subjectInfoForm").validate({
    rules: {
        subjectName: {
            required: true,
            minlength: 3,
            nameVal: true
        }
    },
    messages: {
        subjectName: {
            required: "Please enter name",
            minlength: "Name be between 3 and 64 characters"
        }
    },
    errorElement: "em",
    errorPlacement: function (error, element) {
        element.parents(".col-sm-5").addClass("has-feedback");
        error.addClass("help-block");
        error.addClass("error");
        if (element.prop("type") === "checkbox") {
            error.insertAfter(element.teacher("label"));
        } else {
            error.insertAfter(element);
        }
        if (!element.next("span")[0]) {
            $("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").insertAfter(element);
        }
    },
    success: function (label, element) {
        if (!$(element).next("span")[0]) {
            $("<span class='glyphicon glyphicon-ok form-control-feedback'></span>").insertAfter($(element));
        }
    },
    highlight: function (element, errorClass, validClass) {
        $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
        $(element).next("span").addClass("glyphicon-remove").removeClass("glyphicon-ok");
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
        $(element).next("span").addClass("glyphicon-ok").removeClass("glyphicon-remove");
    }

});

jQuery.validator.addMethod("nameVal", function(value, element) {
    return this.optional(element) || /^[а-яА-ЯёЁa-zA-Z-]{3,64}$/.test(value);
}, 'Name should consist only of letters and hyphens');

