Vue.component('modal', {
    template: '#modal-template'
});
var teacherApp = new Vue({
    el: '#app',
    data: {
        subjectId: '',
        nickname: '',
        firstName: '',
        lastName: '',
        patronymic: '',
        email: '',
        phoneNumber: '',
        password: '',
        description: '',
        teachers: [],
        passwordSec: '',
        showModal: false,
        id: null,
        isEdit: false,
        isAdd: false,
        header:header,

    },
    methods: {
        deleteTeacher: function (teacher) {
            function postConfirmAction() {
                teacherApp.$http.delete("/teachers/" + teacher.id);
                var index = teacherApp.teachers.indexOf(teacher);
                teacherApp.teachers.splice(index, 1);
            }
            this.confirmDelete(postConfirmAction);

        },
        confirmDelete: function (onConfirm) {
            $.confirm({
                columnClass: 'medium',
                title: '',
                content: '<span class="confirm-default-text">Do you want to delete this teacher? You can lose your changes.</span>',
                buttons: {
                    confirm: onConfirm,
                    cancel: function () {}
                }
            });
        },
        editTeacher: function (teacher) {
            this.isAdd = true;
            this.isEdit = true;
            this.id = teacher.id;
            this.nickname = teacher.nickname;
            this.firstName = teacher.firstName;
            this.lastName = teacher.lastName ;
            this.patronymic = teacher.patronymic;
            this.email = teacher.email ;
            this.phoneNumber = teacher.phoneNumber;
            this.description = teacher.description;
            var index = teacherApp.teachers.indexOf(teacher);
            teacherApp.teachers.splice(index, 1);
        },
        addTeacher: function () {
            this.isAdd = true;
        },
        saveTeacher: function () {
            var teach = {
                id: this.id,
                nickname: this.nickname,
                firstName: this.firstName,
                lastName: this.lastName,
                patronymic: this.patronymic,
                email: this.email,
                phoneNumber: this.phoneNumber,
                description: this.description
            };
            if (this.id == null) {
                this.$http.post('/teachers', teach).then(function (response) {
                        teacherApp.teachers.push(teach);
                    },
                    function (response) {
                        //todo handle error
                    });
            }
            else {
                this.$http.put('/teachers', teach).then(function (response) {
                        teacherApp.teachers.push(teach);
                    },
                    function (response) {
                        //todo handle error
                    });
            }
            this.id = null;
            this.nickname = null;
            this.firstName = null;
            this.lastName = null;
            this.patronymic = null;
            this.email = null;
            this.phoneNumber = null;
            this.description = null;
            this.password = null;
            this.passwordSec = null;
            this.isAdd = false;
        }
    },
    created:function () {
        header.currentPage = "teachers";
    },
    mounted: function () {
        this.$http.get('/teacher').then(function (response) {
                this.teachers = response.body;
            },
            function (response) {
                alert("Can't load data from the database");
            }
        );
    }

});


$.validator.setDefaults( {
    submitHandler: function () {
        teacherApp.saveTeacher();
    }
} );

$("#teacherInfoForm").validate({
    rules: {
        lastName: {
            required: true,
            minlength: 3,
            usernameVal: true
        },
        firstName: {
            required: true,
            minlength: 3,
            usernameVal: true
        },
        patronymic: {
            required: false,
            patronymicVal: true
        },
        nickname: {
            required: true,
            nicknameVal: true,
            minlength: 2
        },
        email: {
            required: true,
            emailVal: true
        },
        phoneNumber: {
            required: true,
            phoneVal: true
        },
    },
    messages: {
        lastName: {
            required: "Please enter last name",
            minlength: "Last name be between 3 and 64 characters"
        },
        firstName: {
            required: "Please enter first name",
            minlength: "First name be between 3 and 64 characters"
        },
        nickname: {
            required: "Please enter a nickname",
            minlength: "Nickname must consist of at least 3 characters"
        },
        email: {
            required: "Please enter a email",
            emailVal: "Please enter a valid email address"
        },
        phoneNumber: {
            required: "Please enter a phone number",
            phoneVal: "Please enter a phone number in XXXXXXXXXX or +XXXXXXXXXXXX format"
        }
    },
    errorElement: "em",
    errorPlacement: function (error, element) {
        element.parents(".col-sm-5").addClass("has-feedback");
        error.addClass("help-block");
        error.addClass("error");
        if (element.prop("type") === "checkbox") {
            error.insertAfter(element.teacher("label"));
        } else {
            error.insertAfter(element);
        }
        if (!element.next("span")[0]) {
            $("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").insertAfter(element);
        }
    },
    success: function (label, element) {
        if (!$(element).next("span")[0]) {
            $("<span class='glyphicon glyphicon-ok form-control-feedback'></span>").insertAfter($(element));
        }
    },
    highlight: function (element, errorClass, validClass) {
        $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
        $(element).next("span").addClass("glyphicon-remove").removeClass("glyphicon-ok");
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
        $(element).next("span").addClass("glyphicon-ok").removeClass("glyphicon-remove");
    }

});

jQuery.validator.addMethod("usernameVal", function(value, element) {
    return this.optional(element) || /^[а-яА-ЯёЁa-zA-Z-]{3,64}$/.test(value);
}, 'Name should consist only of letters and hyphens');

jQuery.validator.addMethod("patronymicVal", function(value, element) {
    return this.optional(element) || /^[а-яА-ЯёЁa-zA-Z']{0,64}$/.test(value);
}, 'Patronymic consist only of letters and apostrophes');

jQuery.validator.addMethod("nicknameVal", function(value, element) {
    return this.optional(element) || /^[a-zA-Z][a-zA-Z0-9-_.]{2,36}$/.test(value);
}, 'Nickname should consist only latin letters, numbers, underscores and dots');

jQuery.validator.addMethod("phoneVal", function(value, element) {
    return this.optional(element) || /^((\+\d{2})?\d{10})?$/.test(value);
});

jQuery.validator.addMethod("emailVal", function(value, element) {
    return this.optional(element) || /^[-a-z0-9!#$%&'*+/=?^_`{|}~]+(\.[-a-z0-9!#$%&'*+/=?^_`{|}~]+)*@([a-z0-9]([-a-z0-9]{0,61}[a-z0-9])?\.)*(aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|[a-z][a-z])$/.test(value);
});
