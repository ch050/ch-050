Vue.component('modal', {
    template: '#modal-template'
});
var lessonApp = new Vue({
    el: '#app',
    data: {
        subjectId: '',
        selectedSubjectName : '',
        selector: "0",
        teacherId: '',
        schoolClassId: '',
        classes: [],
        schoolClasses: [],
        teachers:[],
        subjects:[],
        lessons:[],
        showModal: false,
        schoolClassSelect: null,
        subjectSelect: null,
        teacherSelect: null,
        id: null,
        isAdd: false,
        header:header,
    },
    created:function () {
      header.currentPage="lessons"
    },
    methods: {
        deleteLesson: function (lesson) {
            function postConfirmAction() {
                lessonApp.$http.delete("/lessons/" + lesson.id);
                var index = lessonApp.lessons.indexOf(lesson);
                lessonApp.lessons.splice(index, 1);
            }
            this.confirmDelete(postConfirmAction);

        },
        confirmDelete: function (onConfirm) {
            $.confirm({
                columnClass: 'medium',
                title: '',
                content: '<span class="confirm-default-text">Do you want to delete lesson? You can lose your changes.</span>',
                buttons: {
                    confirm: onConfirm,
                    cancel: function () {}
                }
            });
        },
        editLesson: function (lesson) {
            this.isAdd = true;
            this.schoolClassId = lesson.schoolClass.id;
            this.teacherId = lesson.teacher.id;
            this.subjectId = lesson.subject.id;
            this.id = lesson.id;
            var index = this.lessons.indexOf(lesson);
            this.lessons.splice(index, 1);
        },
        addLesson: function () {
            this.isAdd = true;
        },
        saveLesson: function () {

            var teacher = {
                id: this.teacherId,
                firstName: "",
                lastName: "",
                patronymic: ""
            };
            var subject = {
                id: this.subjectId,
                name: ""
            };
            var schoolClass = {
                id: this.schoolClassId,
                name: ""
            };
            var lesson = {
                id: this.id,
                teacher: teacher,
                subject: subject,
                schoolClass: schoolClass
            };
            if (this.id === null) {
                this.$http.post('/lessons', lesson).then(function (response) {

                }, function (response) {
                    alert("This lesson already exists")
                });
            }
            else {
                this.$http.put('/lessons', lesson).then(function (response) {

                }, function (response) {
                    alert("This lesson already exists")
                });
            }
            let lesson1 = {teacher:lessonApp.teachers.filter( teacher => teacher.id === lessonApp.teacherId)[0],
                subject: lessonApp.subjects.filter( subject => subject.id === lessonApp.subjectId)[0],
                schoolClass:lessonApp.schoolClasses.filter(clas => clas.id===lessonApp.schoolClassId)[0]
            };
            lessonApp.lessons.push(lesson1);
            this.isAdd = false;
            this.id = null;
            this.subjectId = null;
            this.schoolClassId = null;
            this.teacherId = null;
        }
    },


    mounted: function () {
        this.$http.get('/lessons').then(function (response) {
                this.lessons = response.body;
            },
            function (response) {
                alert("Can't load data from the database");
            }
        );
        this.$http.get('/subject').then(function (response) {
                this.subjects = response.body;
            },
            function (response) {
                alert("Can't load data from the database");
            }
        );
        this.$http.get('/teacher').then(function (response) {
                this.teachers = response.body;
            },
            function (response) {
                alert("Can't load data from the database");
            }
        );
        this.$http.get('/lessons/schoolClass').then(function (response) {
                this.schoolClasses = response.body;
            },
            function (response) {
                alert("Can't load data from the database");
            }
        );
    }

});


$.validator.setDefaults( {
    submitHandler: function () {
        lessonApp.saveLesson();
    }
} );

$("#lessonInfoForm").validate({
    rules: {
        teacherSelect: {
            required: true,
        },
        subjectSelect: {
            required: true,
        },
        schoolClassSelect: {
            required: true,
        }
    },
    messages: {
        teacherSelect: {
            required: "Please select teacher",
        },
        subjectSelect: {
            required: "Please select subject",
        },
        schoolClassSelect: {
            required: "Please select school class",
        }
    },
    errorElement: "em",
    errorPlacement: function (error, element) {
        error.addClass("help-block");
        element.parents(".col-sm-5").addClass("has-feedback");
        if (element.prop("type") === "checkbox") {
            error.insertAfter(elemen("label"));
        } else {
            error.insertAfter(element);
        }
        if (!element.next("span")[0]) {
            $("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").insertAfter(element);
        }
    },
    success: function (label, element) {
        if (!$(element).next("span")[0]) {
            $("<span class='glyphicon glyphicon-ok form-control-feedback'></span>").insertAfter($(element));
        }
    },
    highlight: function (element, errorClass, validClass) {
        $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
        $(element).next("span").addClass("glyphicon-remove").removeClass("glyphicon-ok");
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
        $(element).next("span").addClass("glyphicon-ok").removeClass("glyphicon-remove");
    }

});



