var parentsApp = new Vue({
    el: '#app',
    data: {
        columns: ['Last name', 'First name', 'Patronymic'],
        parents: []
    },
    methods: {
        editParent: function (parent) {
            var parentId = parent.id;
            console.log(parentId);
            window.sessionStorage.setItem('parentId', parentId);
            window.location = '/parents/editParent';
        },
        deleteParent: function (parent) {
            function postConfirmAction() {
                var parentId = parent.id;
                parentsApp.$http.post('/parents/deactivate', { parentId: parentId }, {
                    emulateJSON: true
                }).then(function (response) {
                    var index = this.parents.indexOf(parent);
                    this.parents.splice(index, 1);
                }, function (response) {
                    alert("Error during deactivating!")
                });
            }
            var parentFullName = parent.lastName + ' ' + parent.firstName + ' ' + parent.patronymic;
            this.confirmDelete(postConfirmAction, parentFullName);

        },
        confirmDelete: function (onConfirm, parentName) {
            $.confirm({
                columnClass: 'medium',
                title: '',
                content: '<span class="confirm-default-text">Do you want to deactivate <span class="confirm-name-text">' + parentName + '</span>?</span>',
                buttons: {
                    confirm: onConfirm,
                    cancel: function () {}
                }
            });
        }
    },
    mounted: function () {
        this.$http.get('/parents/getAll').then(function (response) {
                this.parents = response.body.parents;
            },
            function (response) {
                alert("Can't load data from the database");
            }
        );
    }

});


