var editParentApp = new Vue({
    el: '#app',
    data: {
        parentId: '',
        parent: {}
    },
    methods: {
        save: function () {
            this.$http.post('/parents/edit', this.parent).then(function (response) {
                window.sessionStorage.removeItem("parentId");
                parent.location = '/parents/all';
            }, function (response) {
                alert("error");
            });

        },
        cancelSaving: function () {
            parent.location = '/parents/all';
        }
    },
    mounted: function () {
        this.parentId = window.sessionStorage.getItem("parentId");

        this.$http.post('/parents/get', {parentId: this.parentId}, {
            emulateJSON: true
        }).then(
            function (response) {
                this.parent = response.body;
            }, function (response) {
                alert("Cannot load parent from the database");
            });
    }
});

$.validator.setDefaults({
    submitHandler: function () {
        editParentApp.save();
    }
});

$("#edit-parent-form").validate({
    rules: {
        parentLastName: {
            required: true,
            minlength: 3,
            username: true
        },
        parentFirstName: {
            required: true,
            minlength: 3,
            username: true
        },
        parentPatronymic: {
            required: false,
            patronymic: true
        },
        parentNickname: {
            required: true,
            nickname: true,
            minlength: 2
        },
        parentEmail: {
            required: true,
            email: true
        },
        parentPhone: {
            required: true,
            phone: true
        },
        parentJob: {
            required: false,
            maxlenght: 255
        }
    },
    messages: {
        parentLastName: {
            required: "Please enter parent last name",
            minlength: "Last name should be between 3 and 64 characters"
        },
        parentFirstName: {
            required: "Please enter parent first name",
            minlength: "First name should be between 3 and 64 characters"
        },
        parentNickname: {
            required: "Please enter a parent nickname",
            minlength: "Nickname must consist of at least 3 characters"
        },
        parentEmail: {
            required: "Please enter a parent email",
            email: "Please enter a valid email address"
        },
        parentPhone: {
            required: "Please enter a phone number",
            phone: "Please enter a valid phone number"
        },
        parentJob: {
            maxlenght: "Job description should be shorter than 255 characters"
        }
    },
    errorElement: "em",
    errorPlacement: function (error, element) {
        error.addClass("help-block");
        element.parents(".col-sm-5").addClass("has-feedback");
        if (element.prop("type") === "checkbox") {
            error.insertAfter(element.parent("label"));
        } else {
            error.insertAfter(element);
        }
        if (!element.next("span")[0]) {
            $("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").insertAfter(element);
        }
    },
    success: function (label, element) {
        if (!$(element).next("span")[0]) {
            $("<span class='glyphicon glyphicon-ok form-control-feedback'></span>").insertAfter($(element));
        }
    },
    highlight: function (element, errorClass, validClass) {
        $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
        $(element).next("span").addClass("glyphicon-remove").removeClass("glyphicon-ok");
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
        $(element).next("span").addClass("glyphicon-ok").removeClass("glyphicon-remove");
    }

});

jQuery.validator.addMethod("username", function (value, element) {
    return this.optional(element) || /^[а-яА-ЯёЁa-zA-Z-]{3,64}$/.test(value);
}, 'Name should consist only of letters and hyphens');

jQuery.validator.addMethod("patronymic", function (value, element) {
    return this.optional(element) || /^[а-яА-ЯёЁa-zA-Z']{0,64}$/.test(value);
}, 'Patronymic consist only of letters and apostrophes');

jQuery.validator.addMethod("nickname", function (value, element) {
    return this.optional(element) || /^[a-zA-Z][a-zA-Z0-9-_.]{2,36}$/.test(value);
}, 'Nickname should consist only latin letters, numbers, underscores and dots');

jQuery.validator.addMethod("phone", function (value, element) {
    return this.optional(element) || /^((\+\d{2})?\d{10})?$/.test(value);
});

jQuery.validator.addMethod("email", function (value, element) {
    return this.optional(element) || /^[-a-z0-9!#$%&'*+/=?^_`{|}~]+(\.[-a-z0-9!#$%&'*+/=?^_`{|}~]+)*@([a-z0-9]([-a-z0-9]{0,61}[a-z0-9])?\.)*(aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|[a-z][a-z])$/.test(value);
});