var classesApp = new Vue({
    el: '#app',
    data: {
        columns: ['School class', 'Curator'],
        schoolClasses: {}
    },
    methods: {
        goToAddClass: function () {
            parent.location = "/classes/addClass";
        },
        editClass: function (schoolClass) {
            var schoolClassId = schoolClass.id;
            window.sessionStorage.setItem('schoolClassId', schoolClassId);
            // parent.location = '/classes/editClass';
        },
        deleteClass: function (schoolClass) {
            function postConfirmAction() {
                var schoolClassId = schoolClass.id;
                classesApp.$http.post('/classes/deactivate', { schoolClassId: schoolClassId }, {
                    emulateJSON: true
                }).then(function (response) {
                    var index = this.schoolClasses.indexOf(schoolClass);
                    this.schoolClasses.splice(index, 1);
                }, function (response) {
                    alert("Error during deactivating!")
                });
            }
            this.confirmDelete(postConfirmAction, schoolClass.name);

        },
        confirmDelete: function (onConfirm, className) {
            $.confirm({
                columnClass: 'medium',
                title: '',
                content: '<span class="confirm-default-text">Do you want to deactivate <span class="confirm-name-text">' + className + '</span>?</span>',
                buttons: {
                    confirm: onConfirm,
                    cancel: function () {}
                }
            });
        }
    },
    mounted: function () {
        this.$http.get('/classes/getAll').then(function (response) {
                this.schoolClasses = response.body.schoolClasses;
            },
            function (response) {
                alert("Can't load data from the database");
            }
        );
    }

});


