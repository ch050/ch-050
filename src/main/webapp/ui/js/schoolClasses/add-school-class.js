var addClassApp = new Vue({
    el: '#app',
    data: {
        schoolClass: {
            name: '',
            curatorId: ''
        },
        teachers: []
    },
    methods: {
        saveClass: function () {
            this.schoolClass.curatorId = this.$refs['teacherId'].value;
            console.log(this.schoolClass);
            this.$http.post('/classes/add', addClassApp.schoolClass, {
                emulateJSON: true
            }).then(function (response) {
                alert("success");
                parent.location = "/classes/all";
            }, function (response) {
                alert("error");
            });

        },
        cancelSaving: function () {
            parent.location = '/classes/all';
        }
    },
    mounted: function () {
        this.$http.get('/teacher').then(function (response) {
                var teachers = response.body;
                for (var i = 0; i < teachers.length; i++) {
                    addClassApp.teachers.push({
                        label: teachers[i].lastName + ' ' + teachers[i].firstName + ' ' + teachers[i].patronymic,
                        value: teachers[i].id
                    })
                }
            },
            function (response) {
                alert("Can't load data from the database");
            }
        )
    }
});

$(function () {
    $("#teachers").autocomplete({
        source: addClassApp.teachers,
        select: function (event, ui) {
            $("#teachers").val(ui.item.label);
            $("#teacherId").val(ui.item.value);
            return false;
        }
    })
});

$.validator.setDefaults({
    submitHandler: function () {
        addClassApp.saveClass();
    }
});

$("#add-class-form").validate({
    rules: {
        className: {
            required: true,
            className: true
        }
    },
    messages: {
        className: {
            required: "Please enter class name"
        }
    },
    errorElement: "em",
    errorPlacement: function (error, element) {
        error.addClass("help-block");
        element.parents(".col-sm-5").addClass("has-feedback");
        if (element.prop("type") === "checkbox") {
            error.insertAfter(element.parent("label"));
        } else {
            error.insertAfter(element);
        }
        if (!element.next("span")[0]) {
            $("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").insertAfter(element);
        }
    },
    success: function (label, element) {
        if (!$(element).next("span")[0]) {
            $("<span class='glyphicon glyphicon-ok form-control-feedback'></span>").insertAfter($(element));
        }
    },
    highlight: function (element, errorClass, validClass) {
        $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
        $(element).next("span").addClass("glyphicon-remove").removeClass("glyphicon-ok");
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
        $(element).next("span").addClass("glyphicon-ok").removeClass("glyphicon-remove");
    }

});

jQuery.validator.addMethod("className", function (value, element) {
    return this.optional(element) || /^\d{1,2}[-а-яА-Яa-zA-Z]{0,2}$/.test(value);
}, 'Class name should consist only of number (with hyphen and letter)');
