var test = new Vue({
    el: '#language-test-app',
    data: {
        page: "test",
        messages: {}
    },
    methods: {
        loadLanguage: function (url) {
            this.$http.post(url, {page: test.page}, {
                emulateJSON: true
            }).then(function (response) {
                test.messages = response.body;
            }, function (error) {
                console.log(error);
            })
        }
    }
});