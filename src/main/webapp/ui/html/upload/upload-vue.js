var upload_modal = new Vue({
    el: '#upload-modal-app',
    data: {
        fileAccept: '',
        maxUploadSize: 0,
        textLimit: 40,
        eventId: '',
        subject: '',
        gridColumns: ['Filename', 'Actions'],
        addedFiles: [],
        renamedFiles: [],
        existingFiles: [],
        deletedFiles: [],
        app1: app1,
        app: app,
        header:header
    },
    created:function () {
        header.currentPage="journal";
    },
    methods: {
        onFileChange: function (e) {
            const files = e.target.files || e.dataTransfer.files;
            if (!files.length) return;

            const filesWithError = [];

            Array.prototype.forEach.call(files, function (file) {
                if (file.size > upload_modal.maxUploadSize) {
                    filesWithError.push(file.name);
                } else upload_modal.addedFiles.push(file);
            });

            if (filesWithError.length) {
                this.filesErrorMessage(filesWithError);
            }

            e.value = '';
        },
        applyTextFilter: function (string) {
            if (string.length > this.textLimit) {
                string = string.substring(0, this.textLimit) + '...';
            }

            return string;
        },
        saveChanges: function () {

            const data = new FormData();

            Array.prototype.forEach.call(this.addedFiles, function (file) {
                data.append('addedFiles', file);
                data.append('filenames', file.name);
            });

            this.deletedFiles.forEach(function (file) {
                data.append('filesForRemove', file.id);
            });

            // this.renamedFiles.forEach(function (file) {
            //     data.append('filesForRename', file.id);
            //     data.append('newNames', file.name);
            // });

            data.append('subject', this.subject);
            data.append('eventId', this.eventId);

            function closeWindow() {
                upload_modal.addedFiles = [];
                $('#close-modal-btn').click();
            }

            this.$http.post('/files/updateFiles', data, {
                emulateJSON: true
            }).then(
                function (response) { closeWindow() },
                function (error) { closeWindow() }
            )

        },
        init: function () {
            this.existingFiles = [];
            this.eventId = app1.lessonEventId;
            this.subject = app.selectedSubjectName;

            const url = '/files/getFiles/';
            this.$http.get(url + this.eventId).then(function (response) {
                upload_modal.existingFiles = response.body.files;
                upload_modal.fileAccept = response.body.fileAccept;
                upload_modal.maxUploadSize = response.body.maxUploadSize;
            }, function (error) {
                console.log(error);
            })
        },
        downloadLink: function (fileId) {
            return '/files/download/' + fileId;
        },
        deleteExistingFile: function (file) {
            function postConfirmAction() {
                upload_modal.deletedFiles.push(file);
                const index = upload_modal.existingFiles.indexOf(file);
                upload_modal.existingFiles.splice(index, 1);
            }

            this.confirmDelete(postConfirmAction, file.name);
        },
        deleteAddedFile: function (file) {
            function postConfirmAction() {
                let index = upload_modal.addedFiles.indexOf(file);
                upload_modal.addedFiles.splice(index, 1);
            }

            this.confirmDelete(postConfirmAction, file.name);
        },
        confirmDelete: function (onConfirm, filename) {
            $.confirm({
                columnClass: 'medium',
                title: '',
                content: '<span class="confirm-default-text" >Do you want to delete file <span class="confirm-filename-text" >' + filename + '</span> ?</span>',
                buttons: {
                    confirm: onConfirm,
                    cancel: function () {}
                }
            });
        },
        filesErrorMessage: function (filenames) {
            let filesPart = '';

            filenames.forEach(function (filename) {
                filesPart += filename + ', ';
            });

            filesPart = '<span class="confirm-filename-text" >' + filesPart.substring(0, filesPart.length - 2) + '</span>';

            const content = filenames.length == 1 ?
                'File: ' + filesPart + ' was not uploaded' :
                'Files: ' + filesPart + ' were not uploaded';
            $.alert({
                columnClass: 'medium',
                title: 'Max file upload size is: ' + this.formatBytes(this.maxUploadSize),
                content: content,
            });
        },
        formatBytes: function (bytes) {
            if (bytes < 1024) return bytes + " Bytes";
            else if (bytes < 1048576) return (bytes / 1024).toFixed(1) + " KB";
            else if (bytes < 1073741824) return (bytes / 1048576).toFixed(1) + " MB";
            else return (bytes / 1073741824).toFixed(1) + " GB";
        }
    }
});