$('body').ready(function() {
    let date_input = $('input[name="date"]'); //our date input has the name "date"
    let container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
    let date = new Date();
    let today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    let options = {
        format: 'yyyy-mm-dd',
        container: container,
        todayHighlight: true,
        autoclose: true,
        endDate: today,
        orientation: "bottom auto"

    };



	$('.input-daterange input').each(function() {
		$(this).datepicker(options).datepicker('setDate', today);
	});
});
