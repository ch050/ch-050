var table = new Vue({
    el: "#tableJournal",
    data: {
        journalData: {},
        arrayOfJournalData: [],
        currentElements: 0,
        quantityOfFullPages: 2,
        quantityPerPage: 10,
        lastNotFullPage: 0,
        ClassName: "",
        SubjectName: "",
        myshow: false,
        header: header


    },

    methods: {

        currInc: function () {
            if (this.currentElements < this.arrayOfJournalData.length - 1) {
                this.currentElements = this.currentElements + 1;


            }

        },
        currDec: function () {
            if (this.currentElements > 0) {
                this.currentElements = this.currentElements - 1;


            }
        },
        createArray: function () {

            this.arrayOfJournalData = [];
            for (var i = 0; i < this.quantityOfFullPages; i++) {
                var element = {};
                element.dates = [];
                element.children = [];
                for (var j = 0; j < this.quantityPerPage; j++) {
                    element.dates.push(this.mergedJournalData.dates[this.quantityPerPage * i + j]);
                }
                for (var child in this.mergedJournalData.children) {

                    var markArray = [];
                    for (var j = 0; j < this.quantityPerPage; j++) {
                        markArray.push(this.mergedJournalData.children[child].marks[this.quantityPerPage * i + j]);
                    }
                    element.children.push({
                        childName: this.mergedJournalData.children[child].childName,
                        marks: markArray
                    });

                }
                this.arrayOfJournalData.push(element);
            }
            if (this.lastNotFullPage > 0) {
                var element = {};
                element.dates = [];
                element.children = [];

                for (var i = 0; i < this.lastNotFullPage; i++) {
                    element.dates.push(this.mergedJournalData.dates[this.quantityPerPage * this.quantityOfFullPages + i]);
                }
                for (var child in this.mergedJournalData.children) {
                    var markArray = [];
                    for (var j = 0; j < this.lastNotFullPage; j++) {
                        markArray.push(this.mergedJournalData.children[child].marks[this.quantityPerPage * this.quantityOfFullPages + j]);
                    }
                    element.children.push({
                        childName: this.mergedJournalData.children[child].childName,
                        marks: markArray
                    });
                }
                this.arrayOfJournalData.push(element);

            }

        },
        getJournal: function () {
            this.$http.post('/teachersJournal', {
                classId: select.classId,
                subjectId: select.subjectId,
                startDate: select.startDate,
                endDate: select.endDate
            }).then(
                function (response) {
                    this.journalData = response.body;

                    this.quantityOfFullPages = Math.floor(this.journalData.typeLessonEvents.length / this.quantityPerPage);

                    this.lastNotFullPage = this.journalData.typeLessonEvents.length - (this.quantityOfFullPages * this.quantityPerPage);
                    this.createArray();
                    table.myshow = true;
                },
                function (error) {
                    console.log("err");
                });
        }
    },
    created: function () {
        header.currentPage = 'teacherJournal'
    },
    computed: {

        selectedClass: function () {
            var className = "";

            for (item of select.lessonResource) {

                for (var claas in item.schoolClasses) {


                    if (item.schoolClasses[claas].id == select.classId) {
                        className = item.schoolClasses[claas].name;


                    }
                }
            }

            return className;
        },
        mergedJournalData: function () {
            var mergJournal = {};
            var arrDates = [];

            for (var dateType in this.journalData.typeLessonEvents) {
                var date = this.journalData.typeLessonEvents[dateType].eventDate + " (" + this.journalData.typeLessonEvents[dateType].lessonEventType + ") ";

                arrDates.push(date);

            }
            mergJournal.dates = arrDates;
            mergJournal.children = this.journalData.childJournals;
            console.log(mergJournal.children);

            return mergJournal;
        }


    },
    watch: {},

    mounted: function () {


    },


});
