var select = new Vue({
    el: "#select",
    data: {
        lessonResourceUrl: "/lessonEvent/lesson",
        lessonResource: [],
        subjectId: "",
        classId: "",
        classes: [],
        selectedSubjectName: "",
        startDate:"",
        endDate:"",
        header: header,
        table: table


    },
    methods: {
        showStuds: function () {

            table.getJournal();

        },
    }, created: function () {
        header.currentPage = 'teacherJournal'
    },
    mounted: function () {
        this.$http.get(this.lessonResourceUrl).then(function (response) {
                this.lessonResource = response.body;

                if (this.lessonResource.length == 1) {
                    this.subjectId = this.lessonResource[0].id;
                    this.classes = this.lessonResource[0].classes;
                    if (app.classes.length == 1) {
                        this.classId = app.classes[0].id;
                    } else {
                        this.classId = '';
                    }

                }
            },


            function (error) {
                console.log("ERR");
            });
        $("#startDate").on(
            "changeDate", () => {
                table.myshow = false;
                this.startDate = $('#startDate').val()
            }
        );
        $("#endDate").on(
            "changeDate", () => {
                table.myshow = false;
                this.endDate = $('#endDate').val()
            }
        );


    },
    watch: {
        subjectId: function (val) {
            table.myshow = false;
            this.classId = '';
            for (item of this.lessonResource) {

                if (item.subject.id == val) {

                    this.classes = item.schoolClasses;
                    this.selectedSubjectName = item.subject.name;
                    table.SubjectName = item.subject.name;
                    if (this.classes.length == 1) {
                        this.classId = this.classes[0].id;
                    }


                }
            }
        }, classId: function () {
            table.myshow = false;
        }
    },


});
var title = new Vue({
    el: "#title",
    data: {
        header: header,
    },
    created: function () {
        header.currentPage = "teacherJournal"
    },
});
