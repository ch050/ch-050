var app = new Vue({
    el: '#app',
    data: {
        subjectId: '',
        selectedSubjectName: '',
        selector: "0",
        subjects: [],
        classId: '',
        classes: [],
        currSubject: {},
        comment: "",
        currentDate: "",
        showSuccAlert: false,
        lessonType: "null",
        lessonTypes: [],
        lessonStatus: header.messages.news,
        header: header,

    },
    methods: {
        myVmodel: function () {
            this.currentDate = $('#date').val();
        }

    },
    computed: {
        selectedClass: function () {

            for (item of this.classes) {
                if (item.id === this.classId) {
                    return item;
                }
            }

        },
        selectedType: function () {
            for (item of this.lessonTypes) {
                if (item.id === this.lessonType) {
                    return item;
                }
            }
        }
    },
    watch: {
        subjectId: function (val) {
            app.classId = '';
            lst.lessonStatus = header.messages.news;
            for (item of this.subjects) {

                if (item.subject.id == val) {

                    this.currSubject = item;
                    if (localStorage.getItem("lesson") === null) {
                        this.classes = item.schoolClasses;
                    }
                    this.selectedSubjectName = item.subject.name;

                    if (this.classes.length == 1) {
                        this.classId = app.classes[0].id;

                    }


                }
            }
        },
        classId: function (val) {
            if (val) {
                if (!app1.isDisEnd) {
                    app1.isDisStart = true;
                    app1.isDisEnd = false;
                }
            }
        }, comment: function (val) {
            app1.set_data();
        }
    },

    mounted: function () {
        if (localStorage.getItem("lesson") === null) {
            this.$http.get("/lessonEvent/lesson").then(function (response) {

                    app.subjects = response.body;
                    this.lessonTypes = this.subjects[0].typeDto;
                    this.lessonTypes.push({id: "null", name: "Common"});

                    if (this.subjects.length == 1) {
                        this.subjectId = this.subjects[0].id;
                        this.classes = this.subjects[0].classes;
                        if (app.classes.length == 1) {
                            this.classId = app.classes[0].id;
                        } else {
                            this.classId = '';
                        }

                    }
                    if (this.lessonTypes.length === 1) this.lessonType = this.lessonTypes[0].id;

                },
                function (error) {
                });
        }
        $("#date").on(
            "changeDate", () => {
                this.currentDate = $('#date').val()
            }
        );


    }, created: function () {
        header.currentPage = "journal";

    }
});
var lst = new Vue({
    el: "#lessonStatus",
    data: {
        header: header,
        lessonStatus: '',
    }, methods: {
        load: function () {
            app1.get_data();
        }, created: function () {
            header.currentPage = "journal"
        }
    }
});
