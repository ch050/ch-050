var app1 = new Vue({
        el: "#app1",
        data: {
            mark: [" ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
            rows: [],
            rowsAbs: [],
            isDisStart: false,
            isDisEnd: false,
            lessonEventId: "",
            lessonStart: false,
            header:header,

        },
        methods: {
            showAlertSucc: function () {
                app.showSuccAlert = false
            },
            beginEvent: function () {
                lst.lessonStatus = header.messages.started;
                this.set_Lesson_data();

                var arrayOfPupils = [];


                this.$http.post('/lessonEvent/createLessonEvent', {
                    classId: app.classId,
                    subjectId: app.subjectId,
                    eventDate: app.currentDate,
                    comment: app.comment,
                    lessonEventTypeId: app.lessonType,
                    completed: false,
                    completionDate: null
                } ).then(
                    function (response) {


                        this.lessonEventId = response.body.lessonEventId;
                        arrayOfPupils = response.body.childDtos;

                        for (var i = 0; i < arrayOfPupils.length; i++) {
                            arrayOfPupils[i].mark = "";
                            arrayOfPupils[i].name = arrayOfPupils[i].lastName + " " + arrayOfPupils[i].firstName;
                        }
                        this.click();

                        if (localStorage.getItem("row") === null) {
                            this.rows = arrayOfPupils;
                        }
                        this.lessonStart = true;
                        $("#subj").addClass("disabledbutton");
                        $("#date").addClass("disabledbutton");
                        $("#form").addClass("disabledbutton");
                        $("#type").addClass("disabledbutton");

                        setTimeout(function () {
                            saveComplited();
                        }, 45 * 60 * 1000);


                    },
                    function (error) {
                        console.log("err");
                    });


            },
            removeRow: function (row) {
                var index = this.rows.indexOf(row)
                this.rows.splice(index, 1);

                this.rowsAbs.push(row);
                this.set_data();
            },
            removeRowAbs: function (row) {
                var index = this.rowsAbs.indexOf(row);
                this.rowsAbs.splice(index, 1);
                this.rows.push(row);
                this.set_data();
            },
            get_data: function () {
                app.subjects = [JSON.parse(localStorage.getItem('subjects'))];
                app.classes = [JSON.parse(localStorage.getItem('currentClass'))];
                app.lessonTypes = [JSON.parse(localStorage.getItem('currentType'))];
                app.lessonType = JSON.parse(localStorage.getItem('selectedType'));
                app.subjectId = JSON.parse(localStorage.getItem('lesson'));
                app.classId = JSON.parse(localStorage.getItem('class'));
                app.currentDate = JSON.parse(localStorage.getItem('date'));
                this.lessonEventId = JSON.parse(localStorage.getItem('lessonEventId'));
                app.comment = JSON.parse(localStorage.getItem('comment'));

                let parsed = JSON.parse(localStorage.getItem('rows'));
                this.rows = parsed ? parsed : [];

                let parsedAbs = JSON.parse(localStorage.getItem('rowsAbs'));
                this.rowsAbs = parsedAbs ? parsedAbs : [];
            },
            set_Lesson_data: function () {
                localStorage.setItem('subjects', JSON.stringify(app.currSubject));
                localStorage.setItem('currentClass', JSON.stringify(app.selectedClass));
                localStorage.setItem('currentType', JSON.stringify(app.selectedType));
                localStorage.setItem('selectedType', JSON.stringify(app.lessonType));
                localStorage.setItem('lessonEventId', JSON.stringify(this.lessonEventId));

                localStorage.setItem('date', JSON.stringify(app.currentDate));
                localStorage.setItem('comment', JSON.stringify(app.comment));
                localStorage.setItem('lesson', JSON.stringify(app.subjectId));
                localStorage.setItem('lessonEventId', JSON.stringify(this.lessonEventId));
                localStorage.setItem('class', JSON.stringify(app.classId));

            },

            set_data: function () {
                localStorage.setItem('rowsAbs', JSON.stringify(this.rowsAbs));
                localStorage.setItem('lessonEventId', JSON.stringify(this.lessonEventId));
                localStorage.setItem('rows', JSON.stringify(this.rows));
                localStorage.setItem('comment', JSON.stringify(app.comment));

            },

            delete_data: function () {
                localStorage.removeItem('rows');
                localStorage.removeItem('subjects');
                localStorage.removeItem('currentClass');
                localStorage.removeItem('currentType');
                localStorage.removeItem('date');
                localStorage.removeItem('comment');
                localStorage.removeItem('lesson');
                localStorage.removeItem('rowsAbs');
                localStorage.removeItem('class')
                localStorage.removeItem('selectedType')
                localStorage.removeItem('lessonEventId')
            },
            saveComplited: function () {
                console.log("this prearray "+this.preArray);
                console.log("this lessonEventId"+this.lessonEventId);
                console.log("this absArray",this.absArray);
                $.confirm({
                    title: 'Confirm!',
                    content: 'Are you sure, that you want to finish lesson?!',
                    buttons: {
                        confirm: function () {
                            lst.lessonStatus = header.messages.finished;

                            let info = {
                                lessonEventId: app1.lessonEventId,
                                childMarkDtos: app1.preArray,
                                absents: app1.absArray,
                                comment: app.comment,
                                completed: true,

                            };
                            $("#app1").addClass("disabledbutton");

                            $("#comment").addClass("disabledbutton");
                            app1.isDisStart = true;
                            app1.isDisEnd = true;

                            app1.delete_data();
                            app1.click();
                            console.log(JSON.stringify([this.lessonEventId]));
                            app1.$http.post('/lessonEvent/create',
                                info
                            ).then(
                                function (response) {
                                    console.log("Succ")
                                },
                                function (error) {
                                    console.log("err");
                                });
                        },
                        cancel: function () {

                        },

                    }
                });




            },
            saveError: function () {
                var info = JSON.stringify({
                    lessonEventId: this.lessonEventId,
                    childMarkDtos: this.preArray,
                    absents: this.absArray,
                    comment: app.comment,
                    completed: false,
                });

                this.delete_data();
                this.$http.post('/lessonEvent/create',
                    info
                ).then(
                    function (response) {
                        console.log("Succ")
                    },
                    function (error) {
                        console.log("err");
                    });
            },
            click: function () {
                this.isDisStart = !this.isDisStart;
                this.isDisEnd = !this.isDisEnd;
            }
        },
        computed: {
            orderedRows: function () {
                return _.sortBy(this.rows, 'name')

            },
            orderedRowsAbs: function () {
                return _.sortBy(this.rowsAbs, 'name')

            },
            preArray: function () {
                var presArray = [];
                for (var i = 0; i < this.rows.length; i++) {
                    presArray[i] = {
                        childId: this.rows[i].id,
                        mark: this.rows[i].mark
                    }
                }
                return presArray;
            },
            absArray: function () {
                var absArr = $.extend(true, [], this.rowsAbs);
                var absPup = [];
                for (var i = 0; i < absArr.length; i++) {
                    absPup.push(absArr[i].id);
                }
                return absPup;
            },
        },
        mounted: function () {
            if (localStorage.getItem("lesson") !== null) {
                this.get_data();
                this.lessonStart = true;
                $("#subj").addClass("disabledbutton");
                $("#date").addClass("disabledbutton");
                $("#form").addClass("disabledbutton");
                $("#type").addClass("disabledbutton");
                this.isDisStart = false;
                this.isDisEnd = true;


                lst.lessonStatus = header.messages.started;

            }
        },created: function () {
            header.currentPage="journal";

        }
    })
    ;
$(window).bind("beforeunload", function (event) {
    //app1.saveError();
    return "You have some unsaved changes";
});
