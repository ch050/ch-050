$('body').ready(function() {
	var date_input = $('input[id="date"]'); //our date input has the name "date"
	var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
	var date = new Date();
	var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    var options = {
        format: 'yyyy-mm-dd',
		container: container,
		todayHighlight: true,
		autoclose: true,
        orientation: "bottom auto"
	};
	date_input.datepicker(options).datepicker('setDate', today);
});
