package com.school.journal.validation;

import org.junit.Assert;
import org.junit.Test;

public class SchoolClassValidationTest {

    @Test
    public void isClassNameCorrect() {
        String name = "1-A";
        Assert.assertTrue(SchoolClassValidation.isClassNameCorrect(name));
    }

}
