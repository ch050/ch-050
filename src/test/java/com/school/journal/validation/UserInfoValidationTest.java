package com.school.journal.validation;

import org.junit.Assert;
import org.junit.Test;

public class UserInfoValidationTest {

    @Test
    public void isNameCorrectTest() {
        String name = "Anna-Maria";
        Assert.assertTrue(UserValidation.isNameCorrect(name));
    }

    @Test
    public void isNicknameCorrectTest() {
        String nickname = "ann.chernivchan";
        Assert.assertTrue(UserValidation.isNicknameCorrect(nickname));
    }

    @Test
    public void isEmailCorrectTest() {
        String email = "annachernivchan@gmail.com";
        Assert.assertTrue(UserValidation.isEmailCorrect(email));
    }

    @Test
    public void isPhoneCorrectTest() {
        String phone = "+380685695721";
        Assert.assertTrue(UserValidation.isPhoneNumberCorrect(phone));
    }

    @Test
    public void isJobCorrectTest() {
        String name = "Some story about job. The end";
        Assert.assertTrue(UserValidation.isJobCorrect(name));
    }
}
